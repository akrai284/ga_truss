/*
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * This program demonstrates the use of the quick sort algorithm.  For
 * more information about this and other sorting algorithms, see
 * http://linux.wku.edu/~lamonml/kb.html
 *
 */

void quickSort(double numbers[], int array_size);
void q_sort(double numbers[], int left, int right);


void sorting_in_ga(double *numbers, double *numbers_old, int NUM_ITEMS, int *IDs)
{

  int i,j,k,allow = 1;

  for (i = 0; i < NUM_ITEMS; i++)
  {		numbers[i] = numbers_old[i];	
		// printf("%3d   %3.4f\n", IDs[i], numbers_old[i]);
  }


  //perform quick sort on array
  quickSort(numbers, NUM_ITEMS);

  // printf("Done with sort.\n");
 
  for(i=0;i<NUM_ITEMS;i++)
  {	
	// allow = 1; 
	for(j=0;j<NUM_ITEMS;j++)
	{	allow = 1;
		if(numbers[i]==numbers_old[j])	// numbers match...
		{
			for(k=0;k<i;k++)
			{	if(j==IDs[k])	
				{	
					allow = 0;	break;
				}
			}
			if(allow!=0)	{	IDs[i] = j;		break; }				
		}
	}
  }

}


void quickSort(double numbers[], int array_size)
{
  q_sort(numbers, 0, array_size - 1);
}



void q_sort(double numbers[], int left, int right)
{
  double pivot, l_hold, r_hold;

  l_hold = left;
  r_hold = right;
  pivot = numbers[left];
  while (left < right)
  {
    while ((numbers[right] >= pivot) && (left < right))
      right--;
    if (left != right)
    {
      numbers[left] = numbers[right];
      left++;
    }
    while ((numbers[left] <= pivot) && (left < right))
      left++;
    if (left != right)
    {
      numbers[right] = numbers[left];
      right--;
    }
  }
  numbers[left] = pivot;
  pivot = left;
  left = l_hold;
  right = r_hold;
  if (left < pivot)
    q_sort(numbers, left, pivot-1);
  if (right > pivot)
    q_sort(numbers, pivot+1, right);
}
