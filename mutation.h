

void binary_mutation(int popsize, int nchrom, double **binpop,double pmut_b)
{
	double r;
	int i,j,y;
	for(i=0;i<popsize;i++)
	{
		for(j=0;j<nchrom;j++)
		{
			r = rg.Random();
			if(r<pmut_b)
			{
				y = (int)binpop[i][j];
				if(y==1)
					binpop[i][j] = 0.0;
				else
					binpop[i][j] = 1.0;
				
			}
		}
	}
}

void real_mutation1(int popsize, int nvar, double **realpop,double pmut_r, double **lim_r)
{
	double y_lower,y_upper,var1,r;
	int i,j;
	for(i=0;i<popsize;i++)
	{
		
		for(j=0;j<nvar;j++)
		{
			r = rg.Random();
			if(r<pmut_r)
			{
				y_lower = lim_r[j][0];
				y_upper = lim_r[j][1];
				r = rg.Random();
				var1 = y_lower + r*(y_upper-y_lower);
				realpop[i][j] = var1;
			}
		}
	}
}

void real_gaussian_mutation(int popsize, int nvar, double** realpop, double pmut_r, double** lim_r)
{
	double y_lower, y_upper, var1, r, gd;
	int i, j,sgn;

	for (i = 0; i < popsize; i++)
	{
		for (j = 0; j < nvar; j++)
		{
			r = rg.Random();
			sgn = (r < 0.5) ? -1 : 1;
			r = rg.Random();
			gd = sgn * exp(-8.0 + 9.0 * r);
			
			r = rg.Random();
			if (r < pmut_r)
			{
				y_lower = lim_r[j][0];
				y_upper = lim_r[j][1];
				var1 = realpop[i][j] + gd;
				if (var1 < y_lower)
					var1 = y_lower;
				if (var1 > y_upper)
					var1 = y_upper;

				realpop[i][j] = var1;
			}
		}
	}
}
