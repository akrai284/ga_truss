#include "nrutil.h";
#include <iostream>;
using namespace std;

int main()
{
	int **a;
	a = imatrix(1, 4, 1, 4);

	for (int i = 1; i <= 4; i++) {
		for (int j = 1; j <= 4; j++) {
			a[i][j] = i + j;
		}
	}
	for (int i = 1; i <=4; i++) {
		for (int j = 1; j <= 4; j++) {
			cout << a[i][j] << " ";
		}
		cout << "\n";
	}
	free_imatrix(a, 1, 4, 1, 4);
	return(0);
}