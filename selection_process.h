#include "sorting_in_ga.h"

void select_process(int popsize, int nvar, int nchrom, double *fittness,double **population,
					int gen,double **select_pop,double *select_fit,double **best_pop,int Nelite,
					double *best_fit,int *IDs)
{
	double sum_fittness,*cumsum_fittness,r,min_fit,*old_fit, *fittness_sorted;
	int i,j,k,y;

	fittness_sorted = dvector(0,popsize-1);
	cumsum_fittness = dvector(0,popsize-1);
	old_fit			= dvector(0,popsize-1);

	// sort the IDs of population vectors based on their fitness...

	for (i = 0; i < popsize; i++)
	{	IDs[i] = i;	
	// printf("%3d   %3.4f\n", i, fittness[i]);	
	}

	// sorting fitnesses and getting the IDs of corresponding population...
	sorting_in_ga(fittness_sorted,fittness,popsize,IDs);

	//for (i = 0; i < popsize; i++)
	//	printf("%3d   %3.4f\n", IDs[i], fittness_sorted[i]);
	
	//getchar();


	//choosing best Nelite population
	for(j=0;j<Nelite;j++)
	{	
		//printf("entering %3d into %3dth best population \n",IDs[popsize-j-1],j);
		for(i=0;i<nvar+nchrom;i++)
		{
			best_pop[j][i] = population[IDs[popsize-j-1]][i];
		}
		best_fit[j] = fittness[IDs[popsize-j-1]];
		//getchar();
	}
	
	



	min_fit = fittness[IDs[0]];
	sum_fittness = 0.0;

	for(i=0;i<popsize;i++)
	{
		old_fit[i] = fittness[i];
//		fittness[i] = pow((fittness[i]-min_fit),(1+log(gen+1)/100.0));
		fittness[i] = (fittness[i]-min_fit);
		sum_fittness += fittness[i];
	}
	if(fabs(sum_fittness)<0.000001)
	{
		sum_fittness = 0.0;
		for(i=0;i<popsize;i++)
		{
			fittness[i] = 1.0;
			sum_fittness+=1.0;
		}
	}

/* old code optimizing loops
	for(i=0;i<popsize;i++) cumsum_fittness[i] = 0.0;
	
	cumsum_fittness[0] = fittness[0];
	for(i=1;i<popsize;i++)
	{
		cumsum_fittness[i] = cumsum_fittness[i-1] + fittness[i];
		
	}
	for(i=0;i<popsize;i++)
		cumsum_fittness[i] = cumsum_fittness[i]/sum_fittness;
*/
       
	
	cumsum_fittness[0] = fittness[0]/sum_fittness;
	for(i=1;i<popsize;i++)
	{
		cumsum_fittness[i] = cumsum_fittness[i-1] + fittness[i]/sum_fittness;
		
	}
	
        for(i=0;i<popsize;i++)
	{
		r = rg.Random();
		y=0;
		for(j=0;j<popsize;j++)
		{
			y+= (r>cumsum_fittness[j]);
			
		}
		for(k=0;k<nvar+nchrom;k++)
			select_pop[i][k] = population[y][k];
		select_fit[i] = old_fit[y];
		
	}
	
	free_dvector(fittness_sorted,0,popsize-1);
	free_dvector(cumsum_fittness,0,popsize-1);
	free_dvector(old_fit,0,popsize-1);


}


void ranking_selection(int popsize, int nvar, int nchrom, double *fittness,double **population,
					   double **select_pop,double *select_fit,double **best_pop,
					   int Nelite,double *best_fit,int *IDs)
{
	double sum_fittness,*cumsum_fittness,r,min_fit, *fittness_sorted;
	double *rank_fittness,SP=2.0;
	int i,j,k,y;

	fittness_sorted = dvector(0,popsize-1);
	cumsum_fittness = dvector(0,popsize-1);
	rank_fittness	= dvector(0,popsize-1);

	// sort the IDs of population vectors based on their fitness...

	for (i = 0; i < popsize; i++)
	{	IDs[i] = i;	
	// printf("%3d   %3.4f\n", i, fittness[i]);	
	}

	// sorting fitnesses and getting the IDs of corresponding population...
	sorting_in_ga(fittness_sorted,fittness,popsize,IDs);


	//choosing best Nelite population
	for(j=0;j<Nelite;j++)
	{	
	
		for(i=0;i<nvar+nchrom;i++)
		{
			best_pop[j][i] = population[IDs[popsize-j-1]][i];
		}
		best_fit[j] = fittness[IDs[popsize-j-1]];
	
	}
	
	for(j=0;j<popsize;j++)
	{
		rank_fittness[IDs[j]] = 2.0 - SP +2.0*(SP-1.0)*j/(popsize-1);
	}



	min_fit = rank_fittness[IDs[0]];
	sum_fittness = 0.0;

	for(i=0;i<popsize;i++)
	{
		
		rank_fittness[i] = (rank_fittness[i]-min_fit);
		sum_fittness += rank_fittness[i];
	}
	if(fabs(sum_fittness)<0.00001)
	{
		sum_fittness = 0.0;
		for(i=0;i<popsize;i++)
		{
			rank_fittness[i] = 1.0;
			sum_fittness+=1.0;
		}
	}
/* Optimizing loop 
	for(i=0;i<popsize;i++) cumsum_fittness[i] = 0.0;
	
	cumsum_fittness[0] = rank_fittness[0];
	for(i=1;i<popsize;i++)
	{
		cumsum_fittness[i] = cumsum_fittness[i-1] + rank_fittness[i];
		
	}
	for(i=0;i<popsize;i++)
		cumsum_fittness[i] = cumsum_fittness[i]/sum_fittness;
*/
        
	cumsum_fittness[0] = rank_fittness[0]/sum_fittness;
	for(i=1;i<popsize;i++)
	{
		cumsum_fittness[i] = cumsum_fittness[i-1] + rank_fittness[i]/sum_fittness;
		
	}

	for(i=0;i<popsize;i++)
	{
		r = rg.Random();
		y=0;
		for(j=0;j<popsize;j++)
		{
			y+= (r>cumsum_fittness[j]);
			
		}
		for(k=0;k<nvar+nchrom;k++)
			select_pop[i][k] = population[y][k];
		select_fit[i] = fittness[y];
		
	}
	
	free_dvector(fittness_sorted,0,popsize-1);
	free_dvector(cumsum_fittness,0,popsize-1);
	free_dvector(rank_fittness,0,popsize-1);

}
