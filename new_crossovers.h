//one point xover
void xover_onepoint(int nvar, double *parent1,double *parent2,double *child1,double *child2)
{
	int j,k;
	double r;

	r=rg.Random();
	k = (int)(r*nvar)+1;
	for(j=0;j<nvar;j++)
	{
		if(j<k)
		{
			child1[j] = parent1[j];
			child2[j] = parent2[j];
		}
		else
		{
			child1[j] = parent2[j];
			child2[j] = parent1[j];
		}
	}
	
}

// two point xover
void xover_twopoint(int nvar,double *parent1,double *parent2,double *child1,double *child2)
{
	int j,k1,k2;
	double r;
	
	r=rg.Random();
	k1 = (int)(r*nvar)+1;
	r=rg.Random();
	k2 = (int)(r*nvar)+1;
	if(k1>k2)
	{
		j = k1;
		k1 = k2;
		k2 = j;
	}
	for(j=0;j<nvar;j++)
	{
		if(j<k1 || j>k2)
		{
			child1[j] = parent1[j];
			child2[j] = parent2[j];
		}
		else
		{
			child1[j] = parent2[j];
			child2[j] = parent1[j];
		}
	}	
}

//uniform xover
void xover_uniform(int nvar,double *parent1,double *parent2,double *child1,double *child2)
{
	int j;
	double r;

		
	for(j=0;j<nvar;j++)
	{
		r=rg.Random();
		if(r<0.5)
		{
			child1[j] = parent1[j];
			child2[j] = parent2[j];
		}
		else
		{
			child1[j] = parent2[j];
			child2[j] = parent1[j];
		}
	}
}
	
	

//xover arithmatic
void xover_arithmatic(int nvar,double *parent1,double *parent2,double *child1,double *child2)
{
	int j;
	double p1,p2,c1,c2,r;

				
	for(j=0;j<nvar;j++)
	{
		r=rg.Random();
		p1			= parent1[j];
		p2			= parent2[j];
//		c1			= r*p1 + (1.0-r)*p2;
//		c2			= (1.0-r)*p1 + r*p2;
		c1			= (p1+p2)/2.0 + r*(p1-p2);
		c2			= (p1+p2)/2.0 - r*(p1-p2);
		child1[j]	= c1;
		child2[j]	= c2;
		
	}	
}

//xover heuristic
void xover_heuristic(int nvar,double *parent1,double *parent2,double *child1,double *child2,
					 double **lim)
{
	int j;
	double r,y_lower,y_upper,best_parent,worst_parent,c1,c2;
		
	r=rg.Random();			
	for(j=0;j<nvar;j++)
	{
		y_lower = lim[j][0];
		y_upper = lim[j][1];
		best_parent		= parent1[j];
		worst_parent	= parent2[j];
		c1			= best_parent + r*(best_parent - worst_parent);
		c2			= best_parent;	
		if(c1<y_lower)
			c1 = y_lower;
		if(c1>y_upper)
			c1 = y_upper;
		child1[j]	=	c1;
		child2[j]	=	c2;
		
	}
		
}

void main_xover(int popsize,int nvar,double **select_pop,double *select_fit,
				double **realpop,double pcross,double **lim,int *x_count,int *random_vector)
{
	double *parent1,*parent2,*child1,*child2,r1,r;
	int i,j,y;

	parent1 = dvector(0,nvar-1);
	parent2 = dvector(0,nvar-1);
	child1  = dvector(0,nvar-1);
	child2  = dvector(0,nvar-1);
	y = 0;
	for(i=0;i<popsize/2;i++)
	{
		for(j=0;j<nvar;j++)
		{
			if(select_fit[random_vector[y]]>select_fit[random_vector[y+1]])		//parent1 is best than parent2
			{
				parent1[j] = select_pop[random_vector[y]][j];
				parent2[j] = select_pop[random_vector[y+1]][j];
			}
			else
			{
				parent2[j] = select_pop[random_vector[y]][j];
				parent1[j] = select_pop[random_vector[y+1]][j];
			}
		}
		r = rg.Random();
		if(r<pcross)
		{
			r1 = rg.Random();

			if(r1<0.2)
			{
				xover_onepoint(nvar,parent1,parent2,child1,child2);
				x_count[0]++;
			}
			else if(r1>=0.2 && r1<0.4)
			{
				xover_twopoint(nvar,parent1,parent2,child1,child2);
				x_count[1]++;
			}
			else if(r1>=0.4 && r1<0.6)
			{
				xover_uniform(nvar,parent1,parent2,child1,child2);
				x_count[2]++;
			}
			else if(r1>=0.6 && r1<0.8)
			{
				xover_arithmatic(nvar,parent1,parent2,child1,child2);
				x_count[3]++;
			}
			else
			{
				xover_heuristic(nvar,parent1,parent2,child1,child2,lim);
				x_count[4]++;
			}
			for(j=0;j<nvar;j++)
			{
				if(child1[j]>lim[j][1])
					child1[j] = lim[j][1];
				if(child1[j]<lim[j][0])
					child1[j] = lim[j][0];
				if(child2[j]>lim[j][1])
					child2[j] = lim[j][1];
				if(child2[j]<lim[j][0])
					child2[j] = lim[j][0];

				realpop[y][j]	=	child1[j];
				realpop[y+1][j]	=	child2[j];
			}

		}
		else
		{
			for(j=0;j<nvar;j++)
			{
				realpop[y][j]	=	select_pop[random_vector[y]][j];
				realpop[y+1][j]	=	select_pop[random_vector[y+1]][j];
			}
		}
		y+=2;
	}
	free_dvector(parent1,0,nvar-1);
	free_dvector(parent2,0,nvar-1);
	free_dvector(child1,0,nvar-1);
	free_dvector(child2,0,nvar-1);
}


////////////////////////
void main_xoverb(int popsize,int nvar,int nchrom,double **select_pop,double *select_fit,
				double **binpop,double pcross,double **lim_b,int *x_count,int *random_vector)
{
	double *parent1,*parent2,*child1,*child2,r1,r;
	int i,j,y;

	parent1 = dvector(0,nchrom-1);
	parent2 = dvector(0,nchrom-1);
	child1  = dvector(0,nchrom-1);
	child2  = dvector(0,nchrom-1);
	y = 0;
	for(i=0;i<popsize/2;i++)
	{
		for(j=0;j<nchrom;j++)
		{
			if(select_fit[random_vector[y]]>select_fit[random_vector[y+1]])		//parent1 is best than parent2
			{
				parent1[j] = select_pop[random_vector[y]][j+nvar];
				parent2[j] = select_pop[random_vector[y+1]][j+nvar];
			}
			else
			{
				parent2[j] = select_pop[random_vector[y]][j+nvar];
				parent1[j] = select_pop[random_vector[y+1]][j+nvar];
			}
		}
		r = rg.Random();
		if(r<pcross)
		{
			r1 = rg.Random();
	
			if(r1<=0.33)
			{
				xover_onepoint(nchrom,parent1,parent2,child1,child2);
				x_count[0]++;
			}
			else if(r1>0.33 && r1<=0.66)
			{
				xover_twopoint(nchrom,parent1,parent2,child1,child2);
				x_count[1]++;
			}
			else 
			{
				xover_uniform(nchrom,parent1,parent2,child1,child2);
				x_count[2]++;
			}
			
			
			for(j=0;j<nchrom;j++)
			{
				if(child1[j]>lim_b[j][1])
					child1[j] = lim_b[j][1];
				if(child1[j]<lim_b[j][0])
					child1[j] = lim_b[j][0];
				if(child2[j]>lim_b[j][1])
					child2[j] = lim_b[j][1];
				if(child2[j]<lim_b[j][0])
					child2[j] = lim_b[j][0];

				binpop[y][j]	=	child1[j];
				binpop[y+1][j]	=	child2[j];
			}

		}
		else
		{
			for(j=0;j<nchrom;j++)
			{
				binpop[y][j]	=	select_pop[random_vector[y]][j+nvar];
				binpop[y+1][j]	=	select_pop[random_vector[y+1]][j+nvar];
			}
		}
		y+=2;
	}
	free_dvector(parent1,0,nchrom-1);
	free_dvector(parent2,0,nchrom-1);
	free_dvector(child1,0,nchrom-1);
	free_dvector(child2,0,nchrom-1);
}
