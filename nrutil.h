#pragma once
static double dsqrarg;
#define DSQR(a) ((dsqrarg=(a)) == 0.0 ? 0.0 : dsqrarg*dsqrarg)

int** imatrix(long nrl, long nrh, long ncl, long nch)
/* allocate a int matrix with subscript range m[nrl..nrh][ncl..nch] */
{
    long i, r = nrh - nrl + 1, c = nch - ncl + 1;
    int** m = new int* [r+nrl];
    for (int i = nrl; i < r+nrl; ++i)
        m[i] = new int[c+ncl];
    return m;
}

double** dmatrix(long nrl, long nrh, long ncl, long nch)
{
    long i, r = nrh - nrl + 1, c = nch - ncl + 1;
    double** m = new double* [r+nrl];
    for (int i = nrl; i < r+nrl; ++i)
        m[i] = new double[c+ncl];
    return m;
}

float** matrix(long nrl, long nrh, long ncl, long nch)
{
    long i, r = nrh - nrl + 1, c = nch - ncl + 1;
    float** m = new float* [r+nrl];
    for (int i = nrl; i < r+nrl; ++i)
        m[i] = new float[c+ncl];
    return m;
}

double* dvector(long nl, long nh)
{
    double* v = new double[nh  + 1];
    return v;
}

float* vector(long nl, long nh)
{
    float* v = new float[nh  + 1];
    return v;
}

int* ivector(long nl, long nh)
{
    int* v = new int[nh  + 1];
    return v;
}


void free_imatrix(int** m, long nrl, long nrh, long ncl, long nch)
{
    long i, r = nrh - nrl + 1, c = nch - ncl + 1;
    for (int i = nrl; i < r+nrl; ++i)
        delete[] m[i];
    delete[] m;

}


void free_dmatrix(double** m, long nrl, long nrh, long ncl, long nch)
{
    long i, r = nrh - nrl + 1, c = nch - ncl + 1;
    for (int i = nrl; i < r+nrl; ++i)
        delete[] m[i];
    delete[] m;

}
void free_matrix(float** m, long nrl, long nrh, long ncl, long nch)
{
    long i, r = nrh - nrl + 1, c = nch - ncl + 1;
    for (int i = nrl; i < r+nrl; ++i)
        delete[] m[i];
    delete[] m;

}
void free_dvector(double* v, long nl, long nh)
{
    delete[] v;
}

void free_vector(float* v, long nl, long nh)
{
    delete[] v;
}

void free_ivector(int* v, long nl, long nh)
{
    delete[] v;
}
