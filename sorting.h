void sort_vector(double *vect,int size,int *id)
{
	int i,j,k;
	double temp;


	for(i=0;i<size;i++)
	{
		for(j=i+1;j<size;j++)
		{
			if(vect[i]>vect[j])
			{
				temp = vect[i];
				vect[i] = vect[j];
				vect[j] = temp;
				k = id[i];
				id[i] = id[j];
				id[j] = k;
			}
		}
	}
}

void sort_vector(int *vect,int size,int *id)
{
	int i,j,k;
	int temp;

	for(i=0;i<size;i++)
	{
		for(j=i+1;j<size;j++)
		{
			if(vect[i]>vect[j])
			{
				temp = vect[i];
				vect[i] = vect[j];
				vect[j] = temp;
				k = id[i];
				id[i] = id[j];
				id[j] = k;
			}
		}
	}
}
void sort_vector(float *vect,int size,int *id)
{
	int i,j,k;
	float temp;

	for(i=0;i<size;i++)
	{
		for(j=i+1;j<size;j++)
		{
			if(vect[i]>vect[j])
			{
				temp = vect[i];
				vect[i] = vect[j];
				vect[j] = temp;
				k = id[i];
				id[i] = id[j];
				id[j] = k;
			}
		}
	}
}

void sort_matrix_by_row(double **mat,int nr,int nc,int *id,int row)
{
	int i,j;
	double *temp;
	

	
	temp = new double[nc];
	for(i=0;i<nc;i++)
		temp[i] = mat[row-1][i];
	sort_vector(temp,nc,id);
	for(i=0;i<nr;i++)
	{
		for(j=0;j<nc;j++)
			temp[j] = mat[i][j];
		for(j=0;j<nc;j++)
			mat[i][j] = temp[id[j]];
	}
	delete []temp;
}


void sort_matrix_by_row(int **mat,int nr,int nc,int *id,int row)
{
	int i,j;
	int *temp;
	

	temp = new int [nc];
	for(i=0;i<nc;i++)
		temp[i] = mat[row-1][i];
	sort_vector(temp,nc,id);
	for(i=0;i<nr;i++)
	{
		for(j=0;j<nc;j++)
			temp[j] = mat[i][j];
		for(j=0;j<nc;j++)
			mat[i][j] = temp[id[j]];
	}
	delete []temp;
}


void sort_matrix_by_col(double **mat,int nr,int nc,int *id,int col)
{
	int i,j;
	double *temp;
	

	temp = new double[nr];
	for(i=0;i<nr;i++)
		temp[i] = mat[i][col-1];
	sort_vector(temp,nr,id);
	for(i=0;i<nc;i++)
	{
		for(j=0;j<nr;j++)
			temp[j] = mat[j][i];
		for(j=0;j<nr;j++)
			mat[j][i] = temp[id[j]];
	}
	delete []temp;
}


void sort_matrix_by_col(int **mat,int nr,int nc,int *id,int col)
{
	int i,j;
	int *temp;
	

	temp = new int [nr];
	for(i=0;i<nr;i++)
		temp[i] = mat[i][col-1];
	sort_vector(temp,nr,id);
	for(i=0;i<nc;i++)
	{
		for(j=0;j<nr;j++)
			temp[j] = mat[j][i];
		for(j=0;j<nr;j++)
			mat[j][i] = temp[id[j]];
	}
	delete []temp;
}

void remove_duplicate(int *vect,int *siz)
{
	int i,j,size;
	int *temp;
	size = *siz;
	temp = new int[size];
	for(i=0;i<size;i++)
		temp[i] = i;
	sort_vector(vect,size,temp);
	temp[0] = vect[0];
	j = 0;
	for(i=0;i<size-1;i++)
	{
		if(vect[i] != vect[i+1])
		{
			temp[j] = vect[i];
			j++;
		}
	}
	temp[j] = vect[i];
	j++;
	for(i=0;i<size;i++)
		vect[i] = 0;
	for(i=0;i<j;i++)
		vect[i] = temp[i];

	*siz = j;
	delete []temp;
	
}
void renumbering(int *vect,int size,int *return_vect)
{
	int i,j;
	int *temp;
	temp = new int[size];
	for(i=0;i<size;i++)
		temp[i] = i;
	sort_vector(vect,size,temp);
	return_vect[0] = 1;
	j = 1;
	
	for(i=0;i<size-1;i++)
	{
		return_vect[i] = j;
		if(vect[i] != vect[i+1])
			j++;
		
	}
	return_vect[i] = j;

	sort_vector(temp,size,return_vect);
	

	delete []temp;
}

