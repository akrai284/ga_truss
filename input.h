/*This is a file to get the input for the GA program*/

//void input(FILE *rep_ptr);

void ignore_commentnsga(FILE *fp);

void slopes(float *slp, float **node, int **ncon, int nchrom);

void input(int *gener,double *pcross,double *pmut_r,double *pmut_b,int *terminate_gen,
		   double **lim_b, double **lim_r, int *obj_opt,int *Nelite,
		   double *error_coeff,double *start_point,int *n_segment)
{
	int i,intdum, **ncon,inpforce,element_status[3];  
	float cc,Elow,Eupp,xrange,yrange,wlow,wupp,tlow,tupp, curv_low, curv_upp, *slp, floatdum;
	float **node,Fupp,Flow; 
	int nhists,Nnode,Nelem,node_v,thick_v,width_v,curvature_v,force_v,nvar,nchrom,popsize,beamtruss_v;
	double dummy1,dummy2,Elastic_Modulus[3];
	FILE *inpga,*inpnode,*inpelem,*inpnelem; 

	inpnelem = fopen("nodeelem.dat","r");
	ignore_commentnsga(inpnelem);		fscanf(inpnelem,"%d",&Nnode);	 
	ignore_commentnsga(inpnelem);		fscanf(inpnelem,"%d",&Nelem);	  
	ignore_commentnsga(inpnelem);		fscanf(inpnelem,"%d",&inpforce);	  
	ignore_commentnsga(inpnelem);		fscanf(inpnelem,"%d",&inpforce);
	ignore_commentnsga(inpnelem);		fscanf(inpnelem,"%d",&nhists);
	ignore_commentnsga(inpnelem);		fscanf(inpnelem,"%d",&nhists);
	ignore_commentnsga(inpnelem);		fscanf(inpnelem,"%d %lf",&element_status[0],&Elastic_Modulus[0]);
	ignore_commentnsga(inpnelem);		fscanf(inpnelem,"%d %lf",&element_status[2],&Elastic_Modulus[2]);
	ignore_commentnsga(inpnelem);		fscanf(inpnelem,"%d %lf",&element_status[1],&Elastic_Modulus[1]);
	ignore_commentnsga(inpnelem);		fscanf(inpnelem,"%d",&nhists);

	fclose(inpnelem);
	inpga = fopen("inputga.dat","r");



	printf("------------------------------------------------------------\n\n\n");

	printf("This is a Single-Objective GA program to solve the constraint problem\n\n");

	printf("-----------------------------------------------------------\n\n");

	printf("Give problem specification\n\n");

	printf("-------------------------------------------------------------\n");

	printf("-------------------------------------------------------------\n");

	/*Asks for number of the variables*/

	printf("Give no. of real and binary-coded variables\n\n");
	ignore_commentnsga(inpga);	fscanf(inpga,"%d %d",&nvar,&nchrom); 
	printf("%3d %3d\n",nvar,nchrom); 


/*Asks for type of the functions*/

	printf("Give type of objective functions\n\n");
	ignore_commentnsga(inpga);	fscanf(inpga,"%d",obj_opt);	printf("%3d\n",*obj_opt); 

/*Asks for number of the individuals in the population*/


	printf("Give Population size (an even no.) \n");
	ignore_commentnsga(inpga);	fscanf(inpga,"%d",&popsize);  printf("%3d\n",popsize); 

/*No. of generations for which the GA will let the population evolve

Too large value will take very long time and very small value will
not let the GA reach the global Pareto front to the problem*/

	printf("Give the no.of generations \n");
	ignore_commentnsga(inpga);	fscanf(inpga,"%d",gener);	printf("%3d\n",*gener); 

	printf("Give the cross-over probability (between 0.5 and 1)\n");
	ignore_commentnsga(inpga);	fscanf(inpga,"%lf",pcross);	printf("%6.5f\n",*pcross); 

	printf("Give the range of the x nodal coordinates\n");
	ignore_commentnsga(inpga);	fscanf(inpga,"%f ",&xrange);	printf("%6.5f \n",xrange); 

	printf("Give the range of the y nodal coordinates\n");
	ignore_commentnsga(inpga);	fscanf(inpga,"%f ",&yrange);	printf("%6.5f \n",yrange); 

	printf("Give the lower and upper limit of widths\n");
	ignore_commentnsga(inpga);	fscanf(inpga,"%f %f",&wlow,&wupp);
	printf("%6.5f  %6.5f\n",wlow,wupp); 

	printf("Give the lower and upper limit of out-of-plane thicknesses\n");
	ignore_commentnsga(inpga);	fscanf(inpga,"%f %f",&tlow,&tupp);
	printf("%6.5f  %6.5f\n",tlow,tupp); 


	printf("Give the lower and upper limits of the curvatures for elements\n");
	ignore_commentnsga(inpga);	fscanf(inpga,"%f %f",&curv_low,&curv_upp);
	printf("%6.5f  %6.5f\n",curv_low,curv_upp); 

	printf("Give the lower and upper limits of input force\n");
	ignore_commentnsga(inpga);	fscanf(inpga,"%f %f",&Flow,&Fupp);
	printf("%6.5f  %6.5f\n",Flow,Fupp); 


	cc = 1.0/(nvar);
	printf("Give the mutation probability for real-coded vectors (between 0 and %f)\n",cc);
	ignore_commentnsga(inpga);	fscanf(inpga,"%lf",pmut_r);	printf("%lf\n",*pmut_r); 

	printf("Give the lower and upper limit of the elastic moduli\n");
	ignore_commentnsga(inpga);	fscanf(inpga,"%f %f",&Elow,&Eupp);
	printf("%6.5f  %6.5f\n",Elow,Eupp); 

	printf("Give the mutation probability for binary strings \n");
	ignore_commentnsga(inpga);	fscanf(inpga,"%lf",pmut_b);	
	printf("%6.5f  \n",*pmut_b);

	printf("Give the minimum number of generation to terminate the program \n");
	ignore_commentnsga(inpga);	fscanf(inpga,"%d",terminate_gen);
	printf("%6d \n",*terminate_gen);

	printf("Give the number of Elite variables\n");
	ignore_commentnsga(inpga);	fscanf(inpga,"%d",Nelite);
	printf("%6d \n",*Nelite);

	printf("Give the number of segment per element\n");
	ignore_commentnsga(inpga);	fscanf(inpga,"%d",n_segment);
	printf("%6d \n",*n_segment);

	if(*obj_opt ==1)
	{
		printf("Give the value of coefficients for the errors \n");
		ignore_commentnsga(inpga);
		fscanf(inpga,"%lf %lf %lf %lf",&error_coeff[0],&error_coeff[1],&error_coeff[2],&error_coeff[3]);
		printf("%lf %lf %lf %lf\n",error_coeff[0],error_coeff[1],error_coeff[2],error_coeff[3]);

	
		printf("give start point\n");
		ignore_commentnsga(inpga);	fscanf(inpga,"%lf %lf",&start_point[0],&start_point[1]);
		printf("%lf %lf",start_point[0],start_point[1]);
	}

	fclose(inpga);
	printf("----------------------------------------------------------------\n");




	/*****************************************************/
	node_v = 2*Nnode;					//end position of all variables in X
	width_v = node_v + Nelem*(*n_segment);			//thick_v to width_v
	thick_v = 1 + width_v;				//node_v to thick_v
	curvature_v =thick_v + 2*Nelem;		//width_v to curvature_v
	force_v = curvature_v + inpforce;	//curvature_v to force_v
	beamtruss_v = force_v + nchrom;
/*********************************************************/


	node = matrix(0,Nnode-1,0,1);

	inpnode = fopen("node.dat","r");
	for(i=0;i<Nnode;i++)
	{
	//   fscanf(inpnode,"%d  %lf  %lf\n",&intdum,&lim_r[2*i][0],&lim_r[2*i+1][0]);
		fscanf(inpnode,"%d  %lf  %lf\n",&intdum,&dummy1,&dummy2);
		lim_r[2*i][0] = dummy1;		lim_r[2*i+1][0] = dummy2;
		node[i][0] = lim_r[2*i][0];		node[i][1] = lim_r[2*i+1][0];
	// printf(" %3d, %6.5f   %6.5f \n",intdum,node[i][0],node[i][1]);

		lim_r[2*i][0]	= lim_r[2*i][0] - 0.5*xrange;
		lim_r[2*i][1]	= lim_r[2*i][0] + xrange;
		lim_r[2*i+1][0] = lim_r[2*i+1][0] - 0.5*yrange;
		lim_r[2*i+1][1]	= lim_r[2*i+1][0] + yrange;
	//	 printf("%5d %6.5f   %6.5f    %6.5f    %6.5f  \n",2*i+1,lim_r[2*i][0],lim_r[2*i][1],lim_r[2*i+1][0],lim_r[2*i+1][1]);
	}
	fclose(inpnode);
//	getchar();

	ncon = imatrix(0,Nelem-1,0,1);
	inpelem = fopen("elem.dat","r");
	for(i=0;i<Nelem;i++)
		fscanf(inpelem,"%d  %d  %d %f\n",&intdum,&ncon[i][0],&ncon[i][1],&floatdum);
	fclose(inpelem);

	for(i=node_v;i<width_v;i++)
	{
		lim_r[i][0]	= wlow;		lim_r[i][1]	= wupp;
//		printf("limits: %d  %6.5f   %6.5f   \n",i,lim_r[i][0],lim_r[i][1]);
	}
//	getch();

	
	for(i=width_v;i<thick_v;i++)
	{
		lim_r[i][0]	= tlow;		lim_r[i][1]	= tupp;
//		printf("limits: %d  %6.5f   %6.5f   \n",i,lim_r[i][0],lim_r[i][1]);
	}
//	getch();


	slp = vector(0,Nelem-1);
	slopes(slp,node,ncon,Nelem);	  // obtained in RADIANS...
	//for(i=0;i<nchrom;i++)	{ printf("slp[%2d]: %3.4f \n",i,slp[i]);		}		getchar();


	// IMPORTANT: SLOPE LIMITS ARE TO BE SET AS TAN(THETA) THOUGH IN THE INPUT, IT WILL BE GIVEN IN 
	// DEGREES...


	for(i=thick_v;i<curvature_v;i++)
	{		
		lim_r[i][0]	= tan(curv_low);
		lim_r[i][1]	= tan(curv_upp);
//		printf("limits: %d  %6.5f   %6.5f   \n",i,lim_r[i][0],lim_r[i][1]);
	}

	for(i=curvature_v;i<force_v;i++)
	{		
		lim_r[i][0]	= Flow;
		lim_r[i][1]	= Fupp;
//		printf("limits: %d  %6.5f   %6.5f   \n",i,lim_r[i][0],lim_r[i][1]);
	}

	for(i=force_v;i<beamtruss_v;i++)
	{
		if(element_status[0] == 1 && element_status[1] == 0 && element_status[2] == 0)
		{
			lim_r[i][0] = 0.0000;
			lim_r[i][1] = 2.0000;
		}
		else if(element_status[0] == 0 && element_status[1] == 1 && element_status[2] == 0)
		{
			lim_r[i][0] = 2.0001;
			lim_r[i][1] = 3.0000;
		}
		else if(element_status[0] == 0 && element_status[1] == 0 && element_status[2] == 1)
		{
			lim_r[i][0] = 3.0001;
			lim_r[i][1] = 3.9990;
		}
		else if(element_status[0] == 1 && element_status[1] == 1 && element_status[2] == 0)
		{
			lim_r[i][0] = 0.0000;
			lim_r[i][1] = 3.0000;
		}
		else if(element_status[0] == 1 && element_status[1] == 0 && element_status[2] == 1)
		{
			lim_r[i][0] = 3.0001;
			lim_r[i][1] = 5.0000;
		}
		else if(element_status[0] == 0 && element_status[1] == 1 && element_status[2] == 1)
		{
			lim_r[i][0] = 2.0001;
			lim_r[i][1] = 3.9990;
		}
		else if(element_status[0] == 1 && element_status[1] == 1 && element_status[2] == 1)
		{
			lim_r[i][0] = 0.0000;
			lim_r[i][1] = 4.0000;
		}
		else 
		{
			printf("error\n");
			getc(stdin);
			exit(1);
		}






	}

	for (i = 0; i < Nelem; i++)
	{	  
		lim_b[i][0] = Elow; lim_b[i][1] = Eupp;
		// printf("%d, %6.5f   %6.5f   \n",i+1,lim_b[i][0],lim_b[i][1]);
	}

	
	// end of reading parameters

	free_matrix(node,0,Nnode-1,0,1);
	free_imatrix(ncon,0,Nelem-1,0,1);
	free_vector(slp,0,Nelem-1);

	return;
}



	void ignore_commentnsga(FILE *fp)
	{
	char ss;
	do{ fscanf(fp,"%c",&ss);
	// printf("%c",ss);
	}while(ss!=':');
	}



	void slopes(float *slp, float **node, int **ncon, int nchrom)
	{

	int i,  eye, jay;
	float pi;
	pi = 3.14159265358979; 

	for(i=0;i<nchrom;i++)
	{
	eye = ncon[i][0]-1;	jay = ncon[i][1]-1;

	if(pow((node[jay][0] - node[eye][0])*(node[jay][0] - node[eye][0]),0.5) < 0.0001)
	{	slp[i] = pi/2;		// vertical slope
	}
	else
	{
	slp[i] = atan((node[jay][1] - node[eye][1])/(node[jay][0] - node[eye][0]));
	}

	// if (slp[i]<0)	{ slp[i] = slp[i] + pi;	}

	// printf("%2d %2d %2d %3.4f %3.4f %3.4f %3.4f %3.4f\n",i, eye, jay, node[eye][0], node[eye][1],node[jay][0], node[jay][1], slp[i]);
	}
	// getchar();
}












