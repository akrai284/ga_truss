#include<iostream>
#include <stdio.h>
#include<stdlib.h>
#include<time.h>
#include <math.h>
#include <string>

using namespace std;

int seed = time(0);


#include "nrutil.h"
#include "ranrotw.cpp"
TRanrotWGenerator rg(seed);       // make instance of random number generator

#include "selection_process.h"
#include "real_crossover.h"
#include "new_crossovers.h"
#include "mutation.h"
//#include "test_fun.h"

#include "input.h"
#include "func-con.h"


void ignore_commentnewga(FILE* fp);

int main()
{

	int	nvar, nchrom, popsize;



	int i, j, y, gen, intdum, * x_count;
	int write_file = 1;
	double r;
	double max_fit;
	time_t start, end;
	double timediff;
	double** lim_b, ** lim_r;

	int obj_opt;

	FILE* fpout, * fitt, * var, * pop_file, * time_file;

	int gener, Nelite, * IDs;
	//	int Nnode,Nelem,inpforce,nhists;											/*No of generations*/	

	//	float cc,Elow,Eupp,xrange,yrange,wlow,wupp,tlow,tupp, curv_low, curv_upp,Flow,Fupp,floatdum;

	double pcross, pmut_b, pmut_r, fit_time;		/*Cross-over Probability*/
										/*Mutation Probability*/
	double break1, * error_coeff, start_point[2];
	int break2 = 1, terminate_gen, n_segment;
	int is_pop_read = 0, is_conv = 0;
	error_coeff = dvector(0, 5);

	fpout = fopen("inputga.dat", "r");
	ignore_commentnsga(fpout);		fscanf(fpout, "%d", &nvar);	fscanf(fpout, "%d", &nchrom);
	ignore_commentnsga(fpout);		fscanf(fpout, "%d", &intdum);
	ignore_commentnsga(fpout);		fscanf(fpout, "%d", &popsize);
	fclose(fpout);

	printf("nvar: %3d, nchrom: %3d, popsize: %3d \n", nvar, nchrom, popsize);
	getchar();

	lim_b = dmatrix(0, nchrom, 0, 1);
	lim_r = dmatrix(0, nvar, 0, 1);



	input(&gener, &pcross, &pmut_r, &pmut_b, &terminate_gen, lim_b, lim_r,
		&obj_opt, &Nelite, error_coeff, start_point, &n_segment);	//read input from file



	fflush(stdin);
	printf("\n Whether to read population from file  (y/n)\n");
	if (getchar() == 'y')
	{
		is_pop_read = 1;
		is_conv = 1;
	}
	else
		is_pop_read = 0;


	r = rg.Random();
	//	printf ("%8.6f \n ", r);
	//	getc(stdin);

	double** population ,**new_population, ** select_pop, * fittness, * select_fit, ** xover_pop;
	double** realpop, ** binpop, ** best_pop, * best_fit, ** lim, * fittness_sorted, *new_fittness;
	double** error_values, ** best_error;
	int* IDs_old, * random_vector, k;
	population = dmatrix(0, popsize - 1, 0, nvar + nchrom - 1);
	new_population = dmatrix(0, popsize - 1, 0, nvar + nchrom - 1);
	select_pop = dmatrix(0, popsize - 1, 0, nvar + nchrom - 1);
	xover_pop = dmatrix(0, popsize - 1, 0, nvar + nchrom - 1);
	realpop = dmatrix(0, popsize - 1, 0, nvar - 1);
	binpop = dmatrix(0, popsize - 1, 0, nchrom - 1);
	best_pop = dmatrix(0, Nelite - 1, 0, nvar + nchrom - 1);
	fittness = dvector(0, popsize - 1);
	new_fittness = dvector(0, popsize - 1);
	fittness_sorted = dvector(0, popsize - 1);
	select_fit = dvector(0, popsize - 1);
	best_fit = dvector(0, Nelite - 1);
	x_count = ivector(0, 4);
	IDs = ivector(0, popsize - 1);
	lim = dmatrix(0, nvar + nchrom - 1, 0, 1);
	error_values = dmatrix(0, popsize - 1, 0, 3);
	IDs_old = ivector(0, popsize - 1);
	best_error = dmatrix(0, Nelite - 1, 0, 3);
	random_vector = ivector(0, popsize - 1);

	for (i = 0; i < 5; i++)
		x_count[i] = 0;
	for (i = 0; i < nvar + nchrom; i++)
	{
		if (i < nvar)
		{
			lim[i][0] = lim_r[i][0];
			lim[i][1] = lim_r[i][1];
		}
		else
		{
			lim[i][0] = lim_b[i - nvar][0];
			lim[i][1] = lim_b[i - nvar][1];
		}
	}

	//initialization of population
	for (i = 0; i < popsize; i++)
	{
		for (j = 0; j < nvar; j++)
		{
			r = rg.Random();
			population[i][j] = lim_r[j][0] + r * (lim_r[j][1] - lim_r[j][0]);
		}
		for (j = 0; j < nchrom; j++)
		{
			r = rg.Random();
			if (r < 0.5)
				population[i][j + nvar] = 0.0;
			else
				population[i][j + nvar] = 1.0;
		}
		random_vector[i] = i;

	}


	/*/CALCULATE FITTNESS FROM OBJECTIVE FUNCTION

	/*********************************************************/
	if (is_pop_read == 1)
	{
		pop_file = fopen("pop_file.dat", "r+");
		for (i = 0; i < popsize; i++)
		{
			for (j = 0; j < nvar + nchrom; j++)
				fscanf(pop_file, "%lf", &population[i][j]);
		}
		fclose(pop_file);
	}

	evaluate_func(nvar, nchrom, popsize, population, fittness, obj_opt, error_values, error_coeff,
		start_point, n_segment);
	break1 = -10000.0;


	gen = 0;

	//generation start
	time(&start);
	while (gen < gener)
	{
		
		//copying to real/bin population for mutation
		for (i = 0; i < popsize; i++)
		{
			for (j = 0; j < nvar; j++)
				realpop[i][j] = population[i][j];
			for (j = 0; j < nchrom; j++)
				binpop[i][j] = population[i][j + nvar];
		}

		//mutation

		if (nvar > 0)													//real mutation
	//		real_mutation(realpop,pmut_r,dim,lim_r);
			real_gaussian_mutation(popsize, nvar, realpop, pmut_r, lim_r);

		if (nchrom > 0)												//binary mutation
			binary_mutation(popsize, nchrom, binpop, pmut_b);


		// combine realpop and binpop in new_population
		for (i = 0; i < popsize; i++)
		{
			for (j = 0; j < nvar; j++)
				new_population[i][j] = realpop[i][j];
			for (j = 0; j < nchrom; j++)
				new_population[i][j + nvar] = binpop[i][j];
		}

		//CALCULATE FITTNESS


		evaluate_func(nvar, nchrom, popsize, new_population, new_fittness, obj_opt, error_values, error_coeff,
			start_point, n_segment);

		//copy back the population

		for (i = 0; i < popsize; i++) {
			if (new_fittness[i] > fittness[i]) {
				for (j = 0; j < nvar + nchrom; j++) {
					population[i][j] = new_population[i][j];
				}
				fittness[i] = new_fittness[i];
			}
		}
		
		//printing the best population for each generation

		sorting_in_ga(fittness_sorted, fittness, popsize, IDs);
		y = IDs[popsize - 1];
		max_fit = fittness[y];

		if (is_conv == 1) {
			fpout = fopen("convhist.dat", "a+");
		//	var = fopen("convvarhist.dat", "a+");
		}
		else
		{
			fpout = fopen("convhist.dat", "w+");
		//	var = fopen("convvarhist.dat", "w+");
			is_conv = 1;
		}
		fprintf(fpout, "%5d  %5d   %3.5f  ", y, (gen + 1), fittness[y]);
		if (obj_opt == 1)
		{
			for (i = 0; i < 4; i++)
				fprintf(fpout, "%12.3lf", error_values[y][i]);
			for (i = 0; i < 4; i++)
				fprintf(fpout, "%8.3lf", error_coeff[i]);
		}
		fprintf(fpout, "\n");
		fclose(fpout);
		//Print var history
		/*for (i = 0; i < nvar + nchrom; i++)
			fprintf(var, "%12.5lf  ", population[IDs[popsize - 1]][i]);
		fprintf(var, "\n");
		fclose(var);
		*/

		fitt = fopen("fittness.dat", "w+");
		var = fopen("var.dat", "w+");
		// rewind(fitt);	rewind(var);
		for (i = 0; i < nvar + nchrom; i++)
			fprintf(var, "%12.5lf  ", population[y][i]);		//writting fittness
		fprintf(fitt, "%10.5lf\n", fittness[y]);
		fclose(fitt);
		fclose(var);

		printf("Generation %4d %10.5lf \n", gen + 1, fittness[y]);

		//Write all 
		fitt = fopen("fittness_all.dat", "w+");
		var = fopen("var_all.dat", "w+");
		for (i = 0; i < popsize; i++) {
			for (j = 0; j < nvar + nchrom; j++)
				fprintf(var, "%12.5lf  ", population[i][j]);		//writting fittness
			fprintf(var, "\n");
			fprintf(fitt, "%10.5lf\n", fittness[i]);
		}
		fclose(fitt);
		fclose(var);

		fit_time = fittness[y];
		//printing end

		//termination
		if (fabs(max_fit) < 0.0001)
			break;

		if ((gen / terminate_gen) == break2)
		{
			if (fabs(break1 - max_fit) < .0001)
			{
				printf("converge");
				getc(stdin);
				break;
			}
			else
				break1 = max_fit;
			break2++;
		}

		//recording population after 10 generation
		if (write_file == (gen / 10) || gen == gener - 1)
		{
			pop_file = fopen("pop_file.dat", "w+");
			for (i = 0; i < popsize; i++)
			{
				for (j = 0; j < nvar + nchrom; j++)
					fprintf(pop_file, "%10.5lf", population[i][j]);
				fprintf(pop_file, "\n");
			}
			fclose(pop_file);
			write_file++;
		}
		//recording end;



		gen++;


		//getchar();


	}//end of generation


	time(&end);
	timediff = difftime(end, start);
	if (is_pop_read == 1)
		time_file = fopen("time_file.dat", "a+");
	else
		time_file = fopen("time_file.dat", "w+");

	fprintf(time_file, "time taken for optimization : %lf\n", timediff);
	fprintf(time_file, "number of generation : %d\n", gen);
	fprintf(time_file, "fittness obtained : %lf\n", fit_time);
	fprintf(time_file, "x_onepoint = %d\nx_twopoint = %d\nx_uniform = %d\nx_arithmatic = %d\nx_hueristic = %d\n", x_count[0], x_count[1], x_count[2], x_count[3], x_count[4]);
	fclose(time_file);
	printf("\n\nend of generation\n");
	printf("\nTime taken for optimization is %lf", timediff);
	getc(stdin);
	fclose(fpout);
//	fclose(fitt);
//	fclose(var);
}


void ignore_commentnewga(FILE* fp)
{
	char ss;
	do {
		fscanf(fp, "%c", &ss);
		// printf("%c",ss);
	} while (ss != ':');
}