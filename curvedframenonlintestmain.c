# include <stdio.h>
# include <math.h>
# include <malloc.h>
# include "nrutil.h"
//# include "ignore_comment.h"
int obj_opt =2;
# include "meshupdate_curved.h"
# include "frame_nonlinear_obj_curved_new.h"

void main()
{
	float *x;

	float *OBJ;
	int i, nfunc,break_status = 0;

	FILE *fp;

	fp = fopen("inputga.dat","r");
	ignore_comment(fp);	
	fscanf(fp,"%d %d",&nvar,&nchrom); printf("%3d %3d\n",nvar,nchrom); 
	printf("nvar: %3d, nchrom: %3d \n",nvar,nchrom);
	fclose(fp);


	x = vector(0,nvar+nchrom-1);
	fp = fopen("var.dat","r");
	for(i=0;i<nvar+nchrom;i++)
	{
		fscanf(fp,"%f",&x[i]);//	printf("%3.4f \n",x[i]);
	}
	fclose(fp);

	nfunc = 1;

	OBJ = vector(0,nfunc-1);
	meshupdate_curved(x,&break_status);

	// make sure print_coord_opt is on in frame_nonlinear_obj_curved.h
	frame_nonlinear_obj_curved(nfunc,OBJ);
	for(i=0;i<nfunc;i++)  {	 printf("OBJ[%2d]: %6.5f \n",i,OBJ[i]); }

	free_vector(x,0,nvar+nchrom-1);
	getch();

}



