#include<iostream>
#include<math.h>
#include<stdlib.h>
#include<stdio.h>

using namespace std;
//#include"nrutil.h"
void split(double **a,double **p)
{

	int j;

	for(j=0;j<2;j++)
	{
		p[0][j] =  a[0][j];
		p[1][j] = (a[0][j]+a[1][j])/2;
		p[2][j] = (a[0][j]+2*a[1][j]+a[2][j])/4 ;
		p[3][j] = (a[0][j]+3*a[1][j]+3*a[2][j]+a[3][j])/8;
		p[4][j] = (a[0][j]+3*a[1][j]+3*a[2][j]+a[3][j])/8;
		p[5][j] = (a[1][j]+2*a[2][j]+a[3][j])/4;
		p[6][j] = (a[2][j]+a[3][j])/2;
		p[7][j] =  a[3][j];
	}

}
int check(double *a1,double *a2,double *a3,double *a4,double *b1,double *b2,double *b3,double *b4)
{
	double max_ax,max_ay,max_bx,max_by,min_ax,min_ay,min_bx,min_by;
	double a,b;


	a =(a1[0]>a2[0])?a1[0]:a2[0];	b =(a3[0]>a4[0])?a3[0]:a4[0];
	max_ax = (a>b)?a:b;
	max_ax = max(max(a1[0],a2[0]),max(a3[0],a4[0]));
	max_bx = max(max(b1[0],b2[0]),max(b3[0],b4[0]));
	max_ay = max(max(a1[1],a2[1]),max(a3[1],a4[1]));
	max_by = max(max(b1[1],b2[1]),max(b3[1],b4[1]));

	min_ax = min(min(a1[0],a2[0]),min(a3[0],a4[0]));
	min_bx = min(min(b1[0],b2[0]),min(b3[0],b4[0]));
	min_ay = min(min(a1[1],a2[1]),min(a3[1],a4[1]));
	min_by = min(min(b1[1],b2[1]),min(b3[1],b4[1]));
	
	if((min_ax>max_bx ))
		return(0);
	else if((max_ax<min_bx))
		return(0);
	else if((min_ay>max_by ))
		return(0);
	else if((max_ay<min_by))
		return(0);
	else
		return(1);
}

void intersection_chk(int *X, int **elem,int Nelem,double *nx,double *ny, double **ZZp,int *break_status)
{
	double P0[2],P1[2],T0[2],T1[2];
	double **A,A1[4][2],**B,**new_x,**new_y,**old_x,**old_y,**aa,**bb,max_ax,max_ay,min_ax,min_ay,error;
	int i,j,k,m,n,l,is_int,r,com_node;
	double theta1,point1[2],point2[2],point3[2],point4[2],L;
	
	A = dmatrix(0,3,0,1);
	B = dmatrix(0,3,0,1);
	new_x = dmatrix(0,50,0,7);
	new_y = dmatrix(0,50,0,7);
	old_x = dmatrix(0,50,0,7);
	old_y = dmatrix(0,50,0,7);

	aa = dmatrix(0,7,0,1);
	bb = dmatrix(0,7,0,1);
//	P0[0] = 0; P0[1] = .25; P1[0] = 1.5; P1[1] = .25; T0[0] = 1; T0[1] = 3; T1[0] = 1; T1[1] = -1;
//	P2[0] = -.5; P2[1] = 0; P3[0] = 2; P3[1] = 0; T2[0] = 1; T2[1] = 1; T3[0] = 1; T3[1] = -1;

	for(m=0;m<Nelem;m++)
	{
		if(X[m]>=2)
			continue;
		P0[0] = nx[elem[m][1]-1];	P0[1] = ny[elem[m][1]-1];
		P1[0] = nx[elem[m][2]-1];	P1[1] = ny[elem[m][2]-1];
		L = sqrt(pow((P1[0]-P0[0]),2) + pow((P1[1]-P0[1]),2));
		theta1 = atan2((P1[1]-P0[1]),(P1[0]-P0[0]));
		T0[0] = L;	T1[0] = L;
		T0[1] = L*(ZZp[m][0]);		T1[1] = L*(ZZp[m][1]);
		point1[0] = 0.0;	point1[1] = 0.0;
		point2[0] = P1[0]-P0[0];	point2[1] = P1[1]-P0[1];
		point4[0] = point2[0]*cos(theta1) + point2[1]*sin(theta1);
		point4[1] = -point2[0]*sin(theta1) + point2[1]*cos(theta1);
		for(i=0;i<2;i++)
		{
				point1[i] = 0.0;
				point2[i] = 0.0 + T0[i]/3;
				point3[i] = point4[i] - T1[i]/3;
				point4[i] = point4[i];
		}
		P1[0] = point1[0];	P1[1] = point1[1];
		point1[0] = point2[0]*cos(theta1) - point2[1]*sin(theta1);
		point1[1] = point2[0]*sin(theta1) + point2[1]*cos(theta1);
		point2[0] = point3[0]*cos(theta1) - point3[1]*sin(theta1);
		point2[1] = point3[0]*sin(theta1) + point3[1]*cos(theta1);
		point3[0] = point4[0]*cos(theta1) - point4[1]*sin(theta1);
		point3[1] = point4[0]*sin(theta1) + point4[1]*cos(theta1);
		for(i=0;i<2;i++)
		{
				A1[0][i] = P1[i] + P0[i];
				A1[1][i] = point1[i] + P0[i];
				A1[2][i] = point2[i] + P0[i];
				A1[3][i] = point3[i] + P0[i];
		}	

		for(r=m+1;r<Nelem;r++)			
		{
			if(X[r]>=2)
				continue;

			for(i=0;i<4;i++)
			{
				for(j=0;j<2;j++)
					A[i][j] = A1[i][j];
			}

			P0[0] = nx[elem[r][1]-1];	P0[1] = ny[elem[r][1]-1];
			P1[0] = nx[elem[r][2]-1];	P1[1] = ny[elem[r][2]-1];
			L = sqrt(pow((P1[0]-P0[0]),2) + pow((P1[1]-P0[1]),2));
			theta1 = atan2((P1[1]-P0[1]),(P1[0]-P0[0]));
			T0[0] = L;	T1[0] = L;
			T0[1] = L*(ZZp[r][0]);		T1[1] = L*(ZZp[r][1]);
			point1[0] = 0.0;	point1[1] = 0.0;
			point2[0] = P1[0]-P0[0];	point2[1] = P1[1]-P0[1];
			point4[0] = point2[0]*cos(theta1) + point2[1]*sin(theta1);
			point4[1] = -point2[0]*sin(theta1) + point2[1]*cos(theta1);
			for(i=0;i<2;i++)
			{
					point1[i] = 0.0;
					point2[i] = 0.0 + T0[i]/3;
					point3[i] = point4[i] - T1[i]/3;
					point4[i] = point4[i];
			}
			P1[0] = point1[0];	P1[1] = point1[1];
			point1[0] = point2[0]*cos(theta1) - point2[1]*sin(theta1);
			point1[1] = point2[0]*sin(theta1) + point2[1]*cos(theta1);
			point2[0] = point3[0]*cos(theta1) - point3[1]*sin(theta1);
			point2[1] = point3[0]*sin(theta1) + point3[1]*cos(theta1);
			point3[0] = point4[0]*cos(theta1) - point4[1]*sin(theta1);
			point3[1] = point4[0]*sin(theta1) + point4[1]*cos(theta1);
			for(i=0;i<2;i++)
			{
					B[0][i] = P1[i] + P0[i];
					B[1][i] = point1[i] + P0[i];
					B[2][i] = point2[i] + P0[i];
					B[3][i] = point3[i] + P0[i];
			}	

			if(elem[m][1] == elem[r][1] || elem[m][1]==elem[r][2])
				com_node = elem[m][1];
			else if(elem[m][2]==elem[r][1] || elem[m][2]==elem[r][2])
				com_node = elem[m][2];
			else
				com_node =0;

			
			for(j=0;j<4;j++)
			{
				old_x[0][j]		=	 A[j][0];
				old_x[0][j+4]	=	 B[j][0];
				old_y[0][j]		=	 A[j][1];
				old_y[0][j+4]	=	 B[j][1];
			}
/*
			FILE *fp;
			fp = fopen("data_point.dat","r");
			
			for(j=0;j<8;j++)
				fscanf(fp,"%lf %lf\n",&old_x[0][j],&old_y[0][j]);
			fclose(fp);
*/
			n=1;
			for(k=0;k<50;k++)
			{	
				i = 0;
				for(l=0;l<n;l++)
				{
					for(j=0;j<4;j++)
					{
						A[j][0]	=	old_x[l][j];
						B[j][0]	=	old_x[l][j+4];	
						A[j][1]	=	old_y[l][j];
						B[j][1]	=	old_y[l][j+4];
					}
				
					split(A,aa);
					split(B,bb);
					is_int=check(aa[0],aa[1],aa[2],aa[3],bb[0],bb[1],bb[2],bb[3]);
					if(is_int == 1)
					{
						for(j=0;j<4;j++)
						{
							new_x[i][j]		=	 aa[j][0];
							new_x[i][j+4]	=	 bb[j][0];
							new_y[i][j]		=	 aa[j][1];
							new_y[i][j+4]	=	 bb[j][1];
						}
						i++;
					}
					is_int=check(aa[0],aa[1],aa[2],aa[3],bb[4],bb[5],bb[6],bb[7]);
					if(is_int == 1)
					{
						for(j=0;j<4;j++)
						{
							new_x[i][j]		=	 aa[j][0];
							new_x[i][j+4]	=	 bb[j+4][0];
							new_y[i][j]		=	 aa[j][1];
							new_y[i][j+4]	=	 bb[j+4][1];
						}
						i++;
					}
					is_int=check(aa[4],aa[5],aa[6],aa[7],bb[0],bb[1],bb[2],bb[3]);
					if(is_int == 1)
					{
						for(j=0;j<4;j++)
						{
							new_x[i][j]		=	 aa[j+4][0];
							new_x[i][j+4]	=	 bb[j][0];
							new_y[i][j]		=	 aa[j+4][1];
							new_y[i][j+4]	=	 bb[j][1];
						
						}
						i++;
					}
					is_int=check(aa[4],aa[5],aa[6],aa[7],bb[4],bb[5],bb[6],bb[7]);
					if(is_int == 1)
					{
						for(j=0;j<4;j++)
						{
							new_x[i][j]		=	 aa[j+4][0];
							new_x[i][j+4]	=	 bb[j+4][0];
							new_y[i][j]		=	 aa[j+4][1];
							new_y[i][j+4]	=	 bb[j+4][1];
						
						}
						i++;
					}
					if(i>40)
						break;
				}
				n = i;
				for(l=0;l<n;l++)
				{
					for(j=0;j<8;j++)
					{
						old_x[l][j] = new_x[l][j];	old_y[l][j] = new_y[l][j];
					}
				}
				error = 0;
				for(l=0;l<n;l++)
				{
					for(j=0;j<4;j++)
					{
						A[j][0]	=	old_x[l][j];
						B[j][0]	=	old_x[l][j+4];	
						A[j][1]	=	old_y[l][j];
						B[j][1]	=	old_y[l][j+4];
					}
					max_ay = max(max(A[0][1],A[1][1]),max(A[2][1],A[3][1]));
					max_ax = max(max(A[0][0],A[1][0]),max(A[2][0],A[3][0]));
					min_ax = min(min(A[0][0],A[1][0]),min(A[2][0],A[3][0]));
					min_ay = min(min(A[0][1],A[1][1]),min(A[2][1],A[3][1]));
					error = error + max((max_ax-min_ax),(max_ay-min_ay));
					
				}
				if(error/(float)n<.005 || i==0 || i>40)   
					break;
//				printf("k = %d i = %d \n",k,n);
			}			//end of k loop

			if(i==0)
				*break_status = 0;

			else if(error/(float)n<.005)
			{
				if(com_node)
				{
					for(l=0;l<n;l++)
					{
						for(j=0;j<4;j++)
						{
							A[j][0]	=	old_x[l][j];
							B[j][0]	=	old_x[l][j+4];	
							A[j][1]	=	old_y[l][j];
							B[j][1]	=	old_y[l][j+4];
						}
						max_ay = max(max(A[0][1],A[1][1]),max(A[2][1],A[3][1]));
						max_ax = max(max(A[0][0],A[1][0]),max(A[2][0],A[3][0]));
					
						if(fabs(max_ax-nx[com_node-1])<.005 && fabs(max_ay-ny[com_node-1])<.005)
						{
							*break_status = 0;
				//			printf("no intersection\n");
						//	continue;
						}
						else
						{
							*break_status = 1;
	//						printf("\n intersection occures(with com_node) %d %d",m+1,r+1);
							break;
						}
					}
				}
				else
				{
					*break_status = 1;
	//				printf("\n intersection occures %d %d",m+1,r+1);
				
				}	
				
			}
			else if(i>40)
			{
				*break_status = 1;
	//			printf("\n intersection occures(i>40) %d %d",m+1,r+1);
		
			}
			

			if(*break_status == 1)
				break;

		}					//end of r loop
		if(*break_status==1)
			break;

	}
/*	for(l=0;l<n;l++)
	{
		for(j=0;j<8;j++)
			printf("%6.2lf",old_x[l][j]);
		printf("\n");
	}
	printf("%d %lf\n",n,error);
*/	free_dmatrix(A,0,3,0,1);
	free_dmatrix(B,0,3,0,1);
	free_dmatrix(new_x,0,50,0,7);
	free_dmatrix(new_y,0,50,0,7);
	free_dmatrix(old_x,0,50,0,7);
	free_dmatrix(old_y,0,50,0,7);
	free_dmatrix(aa,0,7,0,1);
	free_dmatrix(bb,0,7,0,1);


}