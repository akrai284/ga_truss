// # include "nrutil.h"
#include "sorting.h"
//int nvar, nchrom;

void ignore_comment(FILE *fp);
void compute_node(double **mid_node,double *mid_slope,double **end_node,
				  double *end_slope,int n_segment);

void new_freeblock(int **Telem, int TNelem,int *forcenode,int fc_n,int *extra_elem,
				   int *extra_elem_n);

void meshupdate_curved(double *X, int *break_status,int nvar, int nchrom, int obj_opt)
{
	double **node,*F1data,**F2data,thick,E0,ppp;
    int **elem,**dispbc,**forces1,**forces2,Nnode,Nelem,Ndispbc1,Nforces1,Nforces2,print_opt=0;

	double **Tnode,**Tx,d_o;
    int **Telem,TNnode,TNelem,TNdispbc1,TNforces1,TNforces2;



	int node_v,width_v,thick_v,curvature_v,force_v,NINCR;
	int entry_stat,x_entry_stat,y_entry_stat,no_histpts;
	double **mid_node,**end_node,*mid_slope,*end_slope;

	int i,j,k,eye,jay;

	FILE *fpi, *fpo;

	d_o = 0.01;
	  
	fpi = fopen("nodeelem.dat","r");	
	if(fpi!=NULL) {
		ignore_comment(fpi);				fscanf(fpi,"%d\n",&Nnode); 			if(print_opt==1) { printf("%4d\n",Nnode);		}
		ignore_comment(fpi);				fscanf(fpi,"%d\n",&Nelem); 			if(print_opt==1) { printf("%4d\n",Nelem);		}	 
		ignore_comment(fpi);				fscanf(fpi,"%d\n",&Ndispbc1);		if(print_opt==1) { printf("%4d\n",Ndispbc1);	}
		ignore_comment(fpi);				fscanf(fpi,"%d\n",&Nforces1);		if(print_opt==1) { printf("%4d\n",Nforces1);	}
		ignore_comment(fpi);				fscanf(fpi,"%d\n",&Nforces2);		if(print_opt==1) { printf("%4d\n",Nforces2);	}
		ignore_comment(fpi);				fscanf(fpi,"%le\n",&thick);			if(print_opt==1) { printf("%6.5f\n",thick);		}
		ignore_comment(fpi);				fscanf(fpi,"%le\n",&E0);			if(print_opt==1) { printf("%6.5f\n",E0);		}
		ignore_comment(fpi);				fscanf(fpi,"%d\n",&no_histpts); 	if(print_opt==1) { printf("%4d\n",no_histpts);  }
		ignore_comment(fpi);				fscanf(fpi,"%d\n",&NINCR); 			if(print_opt==1) { printf("%4d\n",NINCR);		}
		fclose(fpi);
	}
	else
	{
		printf("error reading nodeelem.dat \n");	getchar();
	}

	if(print_opt == 1)					{	getchar(); }
 
	node = dmatrix(0,Nnode-1,0,2); 
	fpi = fopen("node.dat","r");
	if(fpi!=NULL) 
	{
		for(i=0;i<Nnode;i++){fscanf(fpi,"%lf %lf %lf\n",&node[i][0],&node[i][1],&node[i][2]); }	
		fclose(fpi);
	}
	else	{	printf("error reading node.dat \n");		getchar();}

    
	if(print_opt==1)	
	for(i=0;i<Nnode;i++)
	{	printf("i: %6.5f,node_x: %6.5f, node_y: %6.5f \n",node[i][0],node[i][1],node[i][2]);	}




	elem = imatrix(0,Nelem-1,0,2); 
	fpi = fopen("elem.dat","r");
	if(fpi!=NULL)
	{
		for(i=0;i<Nelem;i++){fscanf(fpi,"%d %d %d %lf\n",&elem[i][0],&elem[i][1],&elem[i][2],&ppp);}
		fclose(fpi);
	}
	else	{	printf("error reading elem.dat \n");		getchar();}
  
	dispbc = imatrix(0,Ndispbc1-1,0,2);
	fpi = fopen("dispbc1.dat","r");
	if(fpi!=NULL) 
	{
		for(i=0;i<Ndispbc1;i++){fscanf(fpi,"%d %d %d \n",&dispbc[i][0],&dispbc[i][1],&dispbc[i][2]);}
		fclose(fpi);
	}
	else	{	printf("error reading dispbc1.dat \n");		getchar();}

	forces1 = imatrix(0,Nforces1-1,0,2); F1data =  dvector(0,Nforces1-1);
	fpi = fopen("forces1.dat","r");
	if(fpi!=NULL)
	{
		for(i=0;i<Nforces1;i++)
			{fscanf(fpi,"%d %d %d %lf\n",&forces1[i][0],&forces1[i][1],&forces1[i][2],&F1data[i]);}	
		fclose(fpi);
	}	
	else	{	printf("error reading forces1.dat \n");		getchar();}
 
	forces2 = imatrix(0,Nforces2-1,0,2); F2data = dmatrix(0,Nforces2-1,0,1);
	fpi = fopen("forces2.dat","r");
	if(fpi!=NULL)
	{
		for(i=0;i<Nforces2;i++)
			{fscanf(fpi,"%d %d %d %lf %lf\n",&forces2[i][0],&forces2[i][1],&forces2[i][2],&F2data[i][0],&F2data[i][1]);}
		fclose(fpi);
	}
	else	{	printf("error reading forces2.dat \n");		getchar();}


/*****************************************************/
	node_v = 2*Nnode;					//end position of all variables in X
	width_v = node_v + Nelem;	//node_v to width_v		width_v -> inplane width
	thick_v = 1 + width_v;				//width_v to thick_v	thick_v -> along Z-direction
	curvature_v =thick_v + 2*Nelem;		//thick_v to curvature_v
	force_v = curvature_v + Nforces1;	//curvature_v to force_v
/*********************************************************/
	
	*break_status=0;

   	// SET THE CURVATURES BEFORE CHANGING THE NODAL COORDINATES
	// compute slopes of elements
	

		// find if some nodes are fixed, if yes, don't change the curvature...

		for(i=0;i<nchrom;i++)
		{	
			entry_stat = 0; x_entry_stat=0; y_entry_stat=0;
			eye = elem[i][1];			jay = elem[i][2];

			for(j=0;j<Ndispbc1;j++)				// find if a node has an entry in dbcs.
			{	
				if(dispbc[j][1]==eye)			// first entry is in dbc
				{	
					 X[thick_v+2*i] = 0;	   // curvature at node 1
					 break;
				}

				if(dispbc[j][1]==jay)			// second entry is in dbc
				{	
					  X[thick_v+2*i + 1] = 0;	   // curvature at node 2					
					  break;
				}

			}
		}

		//getchar();


	// NOW update the nodal coordinates from X
	// make sure that the coordinates for fixed nodes, input force and output displacement DO NOT CHANGE
	for(i=0;i<Nnode;i++)
	{	
		entry_stat = 0; x_entry_stat=0; y_entry_stat=0;
		for(j=0;j<Ndispbc1;j++)				// find if a node has an entry in dbcs.
		{	
			if(dispbc[j][1]==i+1)			// node i+1 has an entry
			{	
				entry_stat = 1;
				if(dispbc[j][2]==1)			{ x_entry_stat = 1;	}	
				if(dispbc[j][2]==2)			{ y_entry_stat = 1;	}
			}
		}

		for(j=0;j<Nforces1;j++)				// find if a node has an entry as input port.
		{	
			if(forces1[j][1]==i+1)			// node i+1 has an entry
			{	
				entry_stat = 1;
				if(forces1[j][2]==1)			{ x_entry_stat = 1;	}	
				if(forces1[j][2]==2)			{ y_entry_stat = 1;	}
			}
		}

		for(j=0;j<Nforces2;j++)				// find if a node has an entry as input port.
		{	
			if(forces2[j][1]==i+1)			// node i+1 has an entry
			{	
				entry_stat = 1;
				if(forces2[j][2]==1)			{ x_entry_stat = 1;	}	
				if(forces2[j][2]==2)			{ y_entry_stat = 1;	}
			}
		}


		if(print_opt==1)	{	printf("ent-stat: %1d, x_stat: %1d, y_stat: %1d \n",entry_stat,x_entry_stat,y_entry_stat);	}

		if(entry_stat!=1)					// node i+1 is not fixed, update nodal coordinates
		{	node[i][1] = X[2*i];	node[i][2] = X[2*i+1];
		}
		else								// node i+1 is fixed, eith x or y or both dofs are fixed
		{	
			if((x_entry_stat==1)&&(y_entry_stat!=1))		// only x is fixed, so update the y coordinate
			{	node[i][2] = X[2*i+1];
			}
			if((x_entry_stat!=1)&&(y_entry_stat==1))	// only y is fixed, so update the x coordinate
			{	node[i][1] = X[2*i];
			}
			// else if(x_entry_stat==1)&&(y_entry_stat==1)	// both x and y are fixed, don't update the coordinates
		}
		if(print_opt==1)	{	printf("i: %6.5f,node_x: %6.5f, node_y: %6.5f \n",node[i][0],node[i][1],node[i][2]);	}
	}
	if(print_opt==1)		{	getchar();	}


	// updated value of thickness
	thick = X[width_v];
	if (print_opt==1)	{	printf("updated value of thickness: %6.5f \n",thick);	}


	// renumbering algorithm to update mesh after removing the non-existing elements
	// make sure to update/record the widths as well

	TNelem = 0;
	for(i=0;i<Nelem;i++)
	{
		if(X[nvar+i]>0)
			TNelem++;
	}







	if(TNelem>0) /*****************************************/
	{
		int fc_n=0,extra_n;
		int *extra_elem, *forcenode;
		Telem = imatrix(0,TNelem-1,0,2);
		extra_elem = ivector(0,TNelem-1);
		forcenode = ivector(0,Nforces1+Nforces2-1);
		for(i=0;i<Nforces1;i++)
			forcenode[i] = forces1[i][1];
		for(i=0;i<Nforces2;i++)
			forcenode[i+Nforces1] = forces2[i][1];
		fc_n = Nforces1+Nforces2;

		TNelem = 0;
		for(i=0;i<Nelem;i++)
		{
			if(X[nvar+i]>0)
			{
				Telem[TNelem][0] = i+1;
				Telem[TNelem][1] = elem[i][1];
				Telem[TNelem][2] = elem[i][2];
				TNelem++;
			}
		}
		new_freeblock(Telem,TNelem,forcenode,fc_n,extra_elem,&extra_n);
		for(i=0;i<extra_n;i++)
		{
			X[nvar+extra_elem[i]-1] = 0;
		}
		free_imatrix(Telem,0,TNelem-1,0,2);
		free_ivector(extra_elem,0,TNelem-1);
		free_ivector(forcenode,0,Nforces1+Nforces2-1);
	}
	else
	{
		*break_status = 1;
		return;
	}
/*****************************************************/
	int *vect,*rvect;
	TNelem = 0;
	FILE *fpp;
	fpp = fopen("global_elem.dat","w+");
	fpo = fopen("Tx.dat","w+");
	for(i=0;i<Nelem;i++)
	{
		if(X[nvar+i]>0.0)
		{
			fprintf(fpo,"%8.5lf %8.5lf %8.5lf %8.5lf\n",X[nvar+i],X[node_v+i],X[thick_v+2*i],X[thick_v+2*i+1]);
			fprintf(fpp,"%d %d \n",TNelem+1,i+1);
			TNelem++;
		}
	}
	fclose(fpo);
	fclose(fpp);
/********************************************************************/
	if(TNelem>0)
	{
		
		int *rand_elem,**node_c_elem;
		
		fpp = fopen("node_c_elem.dat","r+");
		node_c_elem = imatrix(0,Nnode-1,0,7);

		rand_elem = ivector(0,Nelem-1);
		for(i=0;i<Nelem;i++)
			rand_elem[i] = X[nvar+i];


		for(j=0;j<Nnode;j++)
		{
			for(k=0;k<8;k++)
			{
				fscanf(fpp,"%d ",node_c_elem[j][k]);
				
				if(node_c_elem[j][k] != 0)
				{
					if(rand_elem[node_c_elem[j][k]-1] == 0)
						node_c_elem[j][k] = 0;
				}
			}
		}
		
/*		for(i=0;i<Nnode;i++)
		{
			for(j=0;j<8;j++)

*/


		for(eye=0;eye<Nnode;eye++)
		{
			new_node_count = (eye-1)*10;
			c_p[0] = node[eye][1];
			c_p[1] = node[eye][2];
			k=0;
			for( i=0;i<8;i++)
			{
				P[i][0] = c_p[0] + L*cos(-pi/8 + pi/4*(i-1));
				P[i][1] = c_p[1] + L*sin(-pi/8 + pi/4*(i-1));

				if(node_c_elem[eye][i] != 0)
				{
					count_e[k] = i+1;
					k++;
				}
			}

			if (k>0)
			{
				for(i=0;i<k;i++)
				{
					node_count[2*i] = count_e[i];
					if(count_e[i]+1>8)
						node_count[2*i+1] = count_e[i]+1-8;
					else
						node_count[2*i+1] = count_e[i]+1;
				}
				int sw;
				sw = 2*k;
				sort_vector(node_count,sw,ID);
				remove_duplicate(node_count,&sw);

				switch(sw)

				{
				case 3:
					{



















		j = 0;
		k = 0;
		for(i=0;i<Nelem;i++)
		{
			if(X[nvar+i]>0)
			{
				vect[j] = elem[i][1];
				vect[j+1] = elem[i][2];
				j+=2;
				Tx[k][0] = X[nvar+i];
				Tx[k][1] = X[node_v+i];
				Tx[k][2] = X[thick_v+2*i];
				Tx[k][3] = X[thick_v+2*i+1];
				k++;
			}
		}
		renumbering(vect,2*TNelem,rvect);
		fpo = fopen("Telem.dat","w+");
		j = 0;
		Telem = imatrix(0,TNelem-1,0,2);
		for(i=0;i<TNelem;i++)
		{
			fprintf(fpo,"%4d %4d %4d %8.3lf\n",i+1,rvect[j],rvect[j+1],E0);
			Telem[i][0] = i+1;
			Telem[i][1] = rvect[j];
			Telem[i][2] = rvect[j+1];
			j+=2;
		}
		fclose(fpo);
		j = 2*TNelem;
		remove_duplicate(vect,&j);
		TNnode = j;
		fpo = fopen("Tnode.dat","w+");
		Tnode = dmatrix(0,TNnode-1,0,2);
		for(i=0;i<j;i++)
		{
			fprintf(fpo,"%4d %8.5lf %8.5lf\n",i+1,node[vect[i]-1][1],node[vect[i]-1][2]);
			Tnode[i][0] = i+1;
			Tnode[i][1] = node[vect[i]-1][1];
			Tnode[i][2] = node[vect[i]-1][2];
		}
		fclose(fpo);

		j = 2*TNelem;
		remove_duplicate(rvect,&j);
		//writing force
		
		k = 1;
		for(i=0;i<Nforces1;i++)
		{
			for(j=0;j<TNnode;j++)
			{
				if(vect[j] == forces1[i][1])
					break;
			}
			if(j<TNnode)
			{
				k++;
			}
		}
		TNforces1 = k-1;

		k = 1;
		for(i=0;i<Nforces2;i++)
		{	
			for(j=0;j<TNnode;j++)
			{
				if(vect[j] == forces2[i][1])
					break;
			}
			if(j<TNnode)
			{	
					k = k+1;			
			}
		}
		TNforces2 = k - 1;

		// generating displacement boundary condition info	
		k = 1;
		for(i=0;i<Ndispbc1;i++)
		{
			for(j=0;j<TNnode;j++)
			{
				if(vect[j] == dispbc[i][1])
					break;
			}
			if(j<TNnode)
			{
					k = k+1;			
			}
		}
		TNdispbc1 = k-1;
		fpo = fopen("Tnodeelem.dat","w");		
		fprintf(fpo,"number of nodes: %3d\n",TNnode);
		fprintf(fpo,"number of elements: %3d\n",TNelem);
		fprintf(fpo,"number of displacement boundary conditions: %3d\n",TNdispbc1);
		fprintf(fpo,"number of input forces: %3d\n",TNforces1);
		fprintf(fpo,"number of output displacements: %3d\n",TNforces2);
		fprintf(fpo,"thick: %6.5f\n",thick);
		fprintf(fpo,"E0: %6.5f\n",E0);
		fprintf(fpo,"number of history points: %3d \n",no_histpts);
		fprintf(fpo,"number of increment : %3d \n",NINCR);
		fclose(fpo);
/*********************************************************************************/

		int **int_con,**new_con,new_n,e,final_element,final_node;
		double **new_node,**new_tx,th1,th2;

		final_element = TNelem*n_segment;
		final_node	= TNnode + TNelem*(n_segment-1);
		int_con		= imatrix(0,TNnode-1,0,1);
		new_con		= imatrix(0,final_element-1,0,2);
		new_node	= dmatrix(0,final_node-1,0,2);
		new_tx		= dmatrix(0,final_element-1,0,3);
		


		e = 0;
		for(i=0;i<TNnode;i++)
		{
			int_con[i][0] = 0;
			int_con[i][1] = 0;
		}
		new_n = 1;
		if(n_segment>1)
		{
			mid_node	= dmatrix(0,n_segment-2,0,1);
			mid_slope	= dvector(0,n_segment-2);
			end_node	= dmatrix(0,1,0,1);
			end_slope	= dvector(0,1);
			for(i=0;i<TNelem;i++)
			{
				eye = Telem[i][1];	jay = Telem[i][2];
				end_node[0][0] = Tnode[eye-1][1];	end_node[0][1] = Tnode[eye-1][2];
				end_node[1][0] = Tnode[jay-1][1];	end_node[1][1] = Tnode[jay-1][2];
				end_slope[0] = Tx[i][2];	end_slope[1] = Tx[i][3];
				compute_node(mid_node,mid_slope,end_node,end_slope,n_segment);
				th1 = atan2((end_node[1][1] - end_node[0][1]),(end_node[1][0] - end_node[0][0]));



				if(int_con[eye-1][1] == 0)
				{
					int_con[eye-1][1] = new_n;
					int_con[eye-1][0] = eye;
					new_con[e][0] = e+1;
					new_con[e][1] = new_n;
					new_con[e][2] = new_n+1;
					new_node[new_n][0] = new_n+1;
					new_node[new_n][1] = mid_node[0][0];
					new_node[new_n][2] = mid_node[0][1];

					th2 = atan2((mid_node[0][1] - end_node[0][1]),(mid_node[0][0] - end_node[0][0]));

					new_tx[e][2] = Tx[i][2] + th1 - th2;
					new_tx[e][3] = mid_slope[0] + th1 - th2;
					new_n++;
					e++;
				}
				else
				{
					new_con[e][0] = e+1;
					new_con[e][1] = int_con[eye-1][1];
					new_con[e][2] = new_n;
					new_node[new_n-1][0] = new_n;
					new_node[new_n-1][1] = mid_node[0][0];
					new_node[new_n-1][2] = mid_node[0][1];

					th2 = atan2((mid_node[0][1] - end_node[0][1]),(mid_node[0][0] - end_node[0][0]));

					new_tx[e][2] = Tx[i][2]+ th1 - th2;
					new_tx[e][3] = mid_slope[0]+ th1 - th2;
					e++;
				}


				for(j=1;j<n_segment-1;j++)
				{
					new_con[e][0] = e+1;
					new_con[e][1] = new_n;
					new_con[e][2] = new_n+1;
					new_node[new_n][0] = new_n+1;
					new_node[new_n][1] = mid_node[j][0];
					new_node[new_n][2] = mid_node[j][1];

					th2 = atan2((mid_node[j][1] - mid_node[j-1][1]),(mid_node[j][0] - mid_node[j-1][0]));

					new_tx[e][2] = mid_slope[j-1]+ th1 - th2;
					new_tx[e][3] = mid_slope[j]+ th1 - th2;
					new_n++;
					e++;
				}

				
				if(int_con[jay-1][1] == 0)
				{
					
					new_con[e][0] = e+1;
					new_con[e][1] = new_n;
					new_con[e][2] = new_n+1;
					new_n++;
					int_con[jay-1][1] = new_n;
					int_con[jay-1][0] = jay;

					th2 = atan2((end_node[1][1] - mid_node[n_segment-2][1]),(end_node[1][0] - mid_node[n_segment-2][0]));

					new_tx[e][2] = mid_slope[n_segment-2]+ th1 - th2;
					new_tx[e][3] = Tx[i][3]+ th1 - th2;
					e++;
					new_n++;
				}
				else
				{
					new_con[e][0] = e+1;
					new_con[e][1] = new_n++;
					new_con[e][2] = int_con[jay-1][1];

					th2 = atan2((end_node[1][1] - mid_node[n_segment-2][1]),(end_node[1][0] - mid_node[n_segment-2][0]));

					new_tx[e][2] = mid_slope[n_segment-2]+ th1 - th2;
					new_tx[e][3] = Tx[i][3]+ th1 - th2;
					e++;
				}
				

			}
			for(i=0;i<TNnode;i++)
			{
				new_node[int_con[i][1]-1][1] = Tnode[int_con[i][0]-1][1];
				new_node[int_con[i][1]-1][2] = Tnode[int_con[i][0]-1][2];
			}
			free_dmatrix(mid_node,0,n_segment-2,0,1);
			free_dvector(mid_slope,0,n_segment-2);
			free_dmatrix(end_node,0,1,0,1);
			free_dvector(end_slope,0,1);
		}
		else
		{
			for(i=0;i<TNelem;i++)
			{
				new_con[i][0] = Telem[i][0];
				new_con[i][1] = Telem[i][1];
				new_con[i][2] = Telem[i][2];

				new_tx[i][0] = Tx[i][0];
				new_tx[i][1] = Tx[i][1];
				new_tx[i][2] = Tx[i][2];
				new_tx[i][3] = Tx[i][3];
			}
			for(i=0;i<TNnode;i++)
			{
				new_node[i][0] = Tnode[i][0];
				new_node[i][1] = Tnode[i][1];
				new_node[i][2] = Tnode[i][2];
				int_con[i][0] = i+1;
				int_con[i][1] = i+1;
			}
		}

		fpo = fopen("NTelem.dat","w");
		for(i=0;i<final_element;i++)
			fprintf(fpo,"%4d %4d %4d %12.3lf\n",i+1,new_con[i][1],new_con[i][2],E0);
		fclose(fpo);

		fpo = fopen("NTnode.dat","w");
		for(i=0;i<final_node;i++)
			fprintf(fpo,"%4d %8.4lf %8.4lf\n",i+1,new_node[i][1],new_node[i][2]);
		fclose(fpo);
		
		k = 0;
		for(i=0;i<Nelem;i++)
		{
			if(X[nvar+i]>0.0)
			{
				for(j=0;j<n_segment;j++)
				{
					new_tx[k][0] = X[nvar+i];
					new_tx[k][1] = X[node_v+n_segment*i+j];
					k++;
				}
			}
		}
		fpo = fopen("NTx.dat","w");
		for(i=0;i<final_element;i++)
			fprintf(fpo,"%10.5lf %10.5lf %10.5lf %10.5lf\n",new_tx[i][0],new_tx[i][1],new_tx[i][2],new_tx[i][3]);
		fclose(fpo);

/***********************************************/
		fpo = fopen("NTforces1.dat","w");
		k = 1;
		for(i=0;i<Nforces1;i++)
		{
			for(j=0;j<TNnode;j++)
			{
				if(vect[j] == forces1[i][1])
					break;
			}
			if(j<TNnode)
			{
//				if(obj_opt==1)
					ppp = F1data[i]/fabs(F1data[i])*X[curvature_v+k-1];
//				else
//					ppp = F1data[i];
				fprintf(fpo," %2d  %3d  %3d  %6.5f\n",k,int_con[rvect[j]-1][1],forces1[i][2],ppp);
				k++;
			}
		}
		
		fclose(fpo);

		fpo = fopen("NTforces2.dat","w");				//writing forces2	
		k = 1;
		for(i=0;i<Nforces2;i++)
		{	
			for(j=0;j<TNnode;j++)
			{
				if(vect[j] == forces2[i][1])
					break;
			}
			if(j<TNnode)
			{	
					fprintf(fpo," %2d  %3d  %3d  %6.5f   %6.5f\n",k,int_con[rvect[j]-1][1],forces2[i][2],F2data[i][0],F2data[i][1]);
					k = k+1;			
			}
		}
	
		fclose(fpo);

		// generating displacement boundary condition info	
		fpo = fopen("NTdispbc1.dat","w");
		k = 1;
		for(i=0;i<Ndispbc1;i++)
		{
			for(j=0;j<TNnode;j++)
			{
				if(vect[j] == dispbc[i][1])
					break;
			}
			if(j<TNnode)
			{
					fprintf(fpo,"%3d  %3d  %3d \n",k,int_con[rvect[j]-1][1],dispbc[i][2]);
					k = k+1;			
			}
		}
		fclose(fpo);
		fpo = fopen("int_con.dat","w+");
		for(i=0;i<TNnode;i++)
			fprintf(fpo,"%d   %d\n",int_con[i][0],int_con[i][1]);
		fclose(fpo);

		fpo = fopen("NTnodeelem.dat","w");		
		fprintf(fpo,"number of nodes: %3d\n",final_node);
		fprintf(fpo,"number of elements: %3d\n",final_element);
		fprintf(fpo,"number of displacement boundary conditions: %3d\n",TNdispbc1);
		fprintf(fpo,"number of input forces: %3d\n",TNforces1);
		fprintf(fpo,"number of output displacements: %3d\n",TNforces2);
		fprintf(fpo,"thick: %6.5f\n",thick);
		fprintf(fpo,"E0: %6.5f\n",E0);
		fprintf(fpo,"number of history points: %3d \n",no_histpts);
		fprintf(fpo,"number of increments : %3d \n",NINCR);
		fclose(fpo);

		free_imatrix(int_con,0,TNnode-1,0,1);
		free_imatrix(new_con,0,final_element-1,0,2);
		free_dmatrix(new_node,0,final_node-1,0,2);
		free_dmatrix(new_tx,0,final_element-1,0,3);
		

		free_dmatrix(Tnode,0,TNnode-1,0,2);
		free_imatrix(Telem,0,TNelem-1,0,2);
		free_dmatrix(Tx,0,TNelem-1,0,3);
	
	}
	else
		*break_status = 1;
	
	
	free_dmatrix(node,0,Nnode-1,0,2);
	free_imatrix(elem ,0,Nelem-1,0,2);
	free_imatrix(dispbc,0,Ndispbc1-1,0,2);
	free_imatrix(forces1,0,Nforces1-1,0,2);
	free_imatrix(forces2,0,Nforces2-1,0,2); 
	free_dmatrix(F2data,0,Nforces2-1,0,1);
	free_dvector(F1data,0,Nforces1-1);
	
	
	
	
}	// end of meshupdate



void compute_node(double **mid_node,double *mid_slope,double **end_node,
				  double *end_slope,int n_segment)
{
	int i;
	double th,nx1,nx2,ny1,ny2,L,*t,temp1,temp2;
	t	=	dvector(0,n_segment-2);

	nx1 = end_node[0][0];	ny1 = end_node[0][1];
	nx2 = end_node[1][0];	ny2 = end_node[1][1];

	th = atan2((ny2-ny1),(nx2-nx1));
	L = pow((nx2-nx1),2) + pow((ny2-ny1),2);
	L = sqrt(L);
	for(i=0;i<n_segment-1;i++)
		t[i] = -1.0 + 2.0/n_segment*(i+1);

	/*
	nx1 = nx11*(1-t)/2 + nx22*(1+t)/2;
            nz1 = (1/8)*(4 - 6*t + 2*t^3)*0 + (L/8)*(t^2-1)*(t-1)*(alp1) + ...
                (1/8)*(4 + 6*t - 2*t^3)*0 + (L/8)*(t^2-1)*(t+1)*(alp2);
	
	  slope = (1/(4*L))*(6*(t^2 -1)*0 + L*(3*t^2 - 2*t -1)*alp1...
                + 6*(1-t^2)*0 + L*(3*t^2 + 2*t -1)*alp2);
	*/

	nx2 = nx2 - nx1;
	ny2 = ny2 - ny1;
	nx1 = nx2*cos(th) + ny2*sin(th);
	ny1 = ny2*cos(th) - nx2*sin(th);
	nx2 = nx1;	ny2 = ny1;
	nx1 = 0.0; ny1 = 0.0;
	for(i=0;i<n_segment-1;i++)
	{
		temp1 = nx1*(1-t[i])/2.0 + nx2*(1+t[i])/2.0;
		temp2 = (L/8.0)*((pow(t[i],2)-1)*(t[i]-1)*end_slope[0] + (pow(t[i],2)-1)*(t[i]+1)*end_slope[1]);
		mid_node[i][0] = temp1*cos(th) - temp2*sin(th) + end_node[0][0];
		mid_node[i][1] = temp1*sin(th) + temp2*cos(th) + end_node[0][1];
		temp1 = 1.0/4.0*((3.0*pow(t[i],2) - 2.0*t[i] -1)*end_slope[0]+
				(3.0*pow(t[i],2) + 2.0*t[i] -1)*end_slope[1]);
		mid_slope[i] = temp1;

	}
		

	
}


}





void new_freeblock(int **Telem, int TNelem,int *forcenode,int fc_n,int *extra_elem, int *extra_elem_n)
{
	int i,j,k,eye,jay,id=1,r_id=0,rt_id;
	int **group_id,*ID,**replace_id,*retain_id;

	group_id = imatrix(0,TNelem-1,0,1);
	replace_id = imatrix(0,TNelem-1,0,1);
	retain_id = ivector(0,TNelem-1);	
	ID = ivector(0,TNelem-1);

	for(i=0;i<TNelem;i++)
	{
		group_id[i][0] = 0;
		group_id[i][1] = 0;
		ID[i] = i;
		
	}

	remove_duplicate(forcenode,&fc_n);
	

//	sort_matrix_by_col(Telem_s,TNelem,3,ID,2);
	group_id[0][0] = Telem[0][0];		group_id[0][1] = 1;
	for(i=0;i<TNelem;i++)
	{
		eye = Telem[i][1];	jay = Telem[i][2];
		for(j=0;j<TNelem;j++)
		{
			if(i!=j)
			{
				if(eye == Telem[j][1] || eye == Telem[j][2] || jay == Telem[j][1] || jay == Telem[j][2])
				{
					if(group_id[i][1] == 0 && group_id[j][1]!=0)
					{
						group_id[i][1] = group_id[j][1];
						group_id[i][0] = Telem[i][0];
					}
					if(group_id[i][1] != 0 && group_id[j][1]==0)
					{
						group_id[j][1] = group_id[i][1];
						group_id[j][0] = Telem[j][0];
					}
					if(group_id[i][1] != 0 && group_id[j][1] != 0)
					{
						if(group_id[j][1] > group_id[i][1])
						{
							
							for(k=0;k<r_id;k++)
							{
								if(replace_id[k][0] == group_id[j][1])
								{
									if(replace_id[k][1] >group_id[i][1])
										replace_id[k][1] =group_id[i][1];

									break;
								}
							}
							if(k==r_id)
							{
								replace_id[r_id][0] = group_id[j][1];
								replace_id[r_id][1] = group_id[i][1];
								r_id++;
							}
							group_id[j][1] = group_id[i][1];
						}
						else if(group_id[j][1] < group_id[i][1])
						{
							
							for(k=0;k<r_id;k++)
							{
								if(replace_id[k][0] ==group_id[i][1])
								{
									if(replace_id[k][1] >group_id[j][1])
										replace_id[k][1] = group_id[j][1];

									break;
								}
							}
							if(k==r_id)
							{
								replace_id[r_id][0] = group_id[i][1];
								replace_id[r_id][1] = group_id[j][1];
								r_id++;
							}
							group_id[i][1] = group_id[j][1];
						}
						
					}
				}
			}
		}
		if(group_id[i][1] == 0)
		{
			id++;
			group_id[i][0] = Telem[i][0];
			group_id[i][1] = id;
		}
	}

	sort_matrix_by_col(replace_id,r_id,2,ID,2);

	for(j=r_id-1;j>=0;j--)
	{
		for(i=0;i<TNelem;i++)
		{
			if(group_id[i][1] == replace_id[j][0])
				group_id[i][1] = replace_id[j][1];
		}
	}

	rt_id = 0;
	for(j=0;j<fc_n;j++)
	{
		for(i=0;i<TNelem;i++)
		{
			if(forcenode[j] == Telem[i][1] || forcenode[j] == Telem[i][2])
			{
				retain_id[rt_id] = group_id[i][1];
				rt_id++;
				
			}
		}
	}
	k = 0;
	for(i=0;i<TNelem;i++)
	{
		for(j=0;j<rt_id;j++)
		{
			if(group_id[i][1] == retain_id[j])
				break;
		}
		if(j == rt_id)
		{
			extra_elem[k] = Telem[i][0];
			k++;
		}
	}
	*extra_elem_n = k;



/*
	for(i=0;i<TNelem;i++)
		printf("%6d%6d%6d%6d\n",Telem[i][0],Telem[i][1],Telem[i][2],group_id[i][1]);

	for(i=0;i<r_id;i++)
		printf("%6d%6d\n",replace_id[i][0],replace_id[i][1]);
*/
	free_imatrix(group_id,0,TNelem-1,0,1);
	free_ivector(ID,0,TNelem-1);
	free_ivector(retain_id,0,TNelem-1);
	free_imatrix(replace_id,0,TNelem-1,0,1);
}


