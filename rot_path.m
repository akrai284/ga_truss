

load histspc_orig.dat
histspc = histspc_orig;

histspc = [0 0 0;histspc];

rot_angle = input('Input the rotation angle in degrees...');

for i = 1:size(histspc,1)
    x = histspc(i,2);   y = histspc(i,3);
    
    R = [cos(rot_angle*pi/180) -sin(rot_angle*pi/180);
        sin(rot_angle*pi/180) cos(rot_angle*pi/180);];
    
    p = R*[x; y];
    
    xn(i) = p(1);
    yn(i) = p(2);
end


plot(xn,yn,'g'); axis equal

fp = fopen('histspc.dat','w');
for i = 2:size(histspc,1)
    fprintf(fp,'%3d  %6.5f  %6.5f \n',i,xn(i),yn(i));
end
fclose(fp);