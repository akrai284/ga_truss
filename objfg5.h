/********************************************************************************
 * objfg5 : computes fourier coeff based obj and fourier coeffs                 *
 * inputs : op_disp, vec of output node X and Y displs, lambda levels, inc_no   *
 *          dg_op_disp_dx, matrix of the deriv of op_disp wrt design            *
 * outputs:  1) err_flg: primary return var, =0 if successful, =1 if not        *
 *           2) obj and iff reqd obj_grad                                       *
 * status : NDM, Under dev., 3/24/04                                            *
 * version: 0.1, 1st appearance                                                 *
 * Notes  :                                                                     *
 ********************************************************************************/
void objfg5(int nterm, int inc_no, double *start_pt, double **p_op_disp,double **fcoeff, double *d_L, double *d_ang)
{ /* start of fn objfg5 */

  /* d_fcoeff: an, bn, An, alfan (nterm x 4)*/

  /* --- local var --- */
  
  int err_flg=0; /* =0 no errors, =1 errors */
  int i=0, j=0, k=0; /* loop indices */
  int x1id=0, y1id=0; /* x and Y indices for a path pt in p_DgPopDx */
  int x2id=0, y2id=0;
  int npts= inc_no+1; /* no of vertices in the faceted closed curve , = no of edges */
  double a0= 0.0; /* 0th Fourier coeff */
  double a_L=0.0; /* cumulative lth for the entire augmented actual path */
  double a_L2=0.0; /* a_L^2 */
  double a_ang0=0.0; /* inclin of the start seg for the actual aug path */
  double x1=0.0, y1=0.0, x2=0.0, y2=0.0; /* posns of prev and curr path pts */
  double dx=0.0, dy=0.0, dl=0.0, dl2=0.0; /* diff betn the prev and curr path pts */
  double l=0.0, p=0.0; /* temp vars for lth and bend ang */
  double angc=0.0; /* temp vars for angles */
  
 
  double sang=0.0, cang=0.0; /* temp vars for sine and cosine of angle */ 
  double CPI = 3.14159265358979;
  /* NR decl */
  double **apath; /* actual path coords - augmented by start point */
  double *lth; /* culmulative arc lth upto curr aug act path point */
  double *bendang; /* bend ang at curr vertex */
  double *ang; /* inclin of curr seg wrt X axis */
  double **a_fcoeff; /* matrix of actual fourier coeffs - same str as d_coeff */
  
    /* NR alloc */
  apath=dmatrix(1, inc_no+2, 1, 2); /* to append start pt at begining and end */
  lth= dvector(1, npts);
  bendang= dvector(1, npts);
  ang= dvector(1, inc_no+2);
  a_fcoeff= dmatrix(1, nterm, 1, 4);
 
 
  /* ini cum qties */
  for(i=1; i<=nterm; i++)
    {
      a_fcoeff[i][1]=0.0;
      a_fcoeff[i][2]=0.0;    
    }

  /* create augmented actual path */
  apath[1][1]= start_pt[0]; /* append start pt at begining */
  apath[1][2]= start_pt[1];
  apath[inc_no+2][1]= start_pt[0]; /* append start pt at end */
  apath[inc_no+2][2]= start_pt[1];
  
  for(i=1; i<=inc_no; i++)
    { /* actual path input from analysis goes from 2 to (inc_no+1) */
      apath[i+1][1]= p_op_disp[i-1][0];
      apath[i+1][2]= p_op_disp[i-1][1];
      x1id= 2*(i-1) +1; y1id= x1id+1;
   
    }
 
  /* --- compute lengths and angles --- */
  for(i=1; i<=npts; i++)
    { 
      x1=apath[i][1];   y1=apath[i][2];   x2=apath[i+1][1]; y2=apath[i+1][2];
      dx= x2-x1;
      dy= y2-y1;
      a_L+= sqrt(DSQR(dx) + DSQR(dy)); /* update total lth */
      lth[i]= a_L; /* update cum lth */

      ang[i]= atan2(dy, dx);

      /* compute corr grads iff reqd */ 
 

    } /* end of computing lengths and angles */
  ang[npts+1]= ang[1]; /* copy first to last as the segs are the same */
  a_ang0=ang[1]; 
  a_L2= DSQR(a_L);
 

  /* --- compute bend angles --- */
  for(i=1; i<=npts; i++)
    {
      bendang[i]= ang[i+1]-ang[i];
      if(bendang[i] > CPI)
	bendang[i]-= 2.0*CPI;
      else
	if(bendang[i] < -1.0*CPI)
	  bendang[i]+= 2.0*CPI; 
    } /* end of computing bend angles */

  /* --- compute Fourier Coeffs --- */
  a0= -1.0*CPI; 
  for(i=1; i<=npts; i++)
    { /* loop over aug act path points to compute primary fourier coeffs, i */
      l= lth[i];
      p= bendang[i];
      a0-= l*p/a_L;
      for(k=1; k<=nterm; k++)
	{ /* loop over no of terms in fourier seq, k */
	  angc= 2.0*CPI*k*l/a_L;
	  sang= sin(angc);
	  cang= cos(angc);
	  a_fcoeff[k][1]-= (p*sang)/(k*CPI);
	  a_fcoeff[k][2]+= (p*cang)/(k*CPI);

	 

	} /* end of loop over no of terms in fourier seq, k=1:nterm */
    } /* end of pri fourier coeff computn , i=1, npts */

  /* compute polar fourier coeffs from the primary ones */
  for(i=1; i<=nterm; i++)
    {
      a_fcoeff[i][3]= sqrt(DSQR(a_fcoeff[i][1]) + DSQR(a_fcoeff[i][2]));
      a_fcoeff[i][4]= atan2(a_fcoeff[i][2], a_fcoeff[i][1]);
   
 
    }


//output
  for(i=1;i<=nterm;i++)
  {
	  for(j=1;j<=4;j++)
	  {
		  fcoeff[i][j] = a_fcoeff[i][j];
	  }
  }
  *d_L = a_L;
  a_ang0 = atan2((p_op_disp[0][1] - p_op_disp[inc_no-1][1]),(p_op_disp[0][0] - p_op_disp[inc_no-1][0]));
  *d_ang = a_ang0;



  /* NR dealloc */
  free_dmatrix(apath, 1, inc_no+2, 1, 2);
  free_dvector(lth, 1, npts);
  free_dvector(bendang, 1, npts);
  free_dvector(ang, 1, inc_no+2);
  free_dmatrix(a_fcoeff, 1, nterm, 1, 4);
//  return(err_flg);

} /* end of fn objfg5 */
void forier_obj(int nterm, int inc_no, double *start_pt, double **p_op_disp,int no_histpts,
				double **spc_point, double *obj,double *err_frame,double *error_coeff)
{

	double err_A=0.0, err_alfa=0.0, err_L=0.0, err_ang0=0.0; /* error comps */
	double c_A; /* 0.5625, 0.6 weight fns for components of the obj */
	double c_a;
	double c_b;
	double c_alfa; /*0.1 2.5e-3; */
	double c_L; /* 2.0e-4; */
	double c_ang0;  /* 100; */
	int i;


	double a_L,a_ang0,d_L,d_ang0,**a_fcoeff,**d_fcoeff;
	double dA,Atol,dalfa,ARTOL = 0.01,err_a,err_b ;
	
	d_fcoeff= dmatrix(1, nterm, 1, 4);
	a_fcoeff= dmatrix(1, nterm, 1, 4);
	c_a = error_coeff[0];
	c_b = error_coeff[1];
	c_L = error_coeff[2];
	c_ang0 = error_coeff[3];
/*
	
	FILE *fp;
	double **actpth,**path;
	actpth = dmatrix(0,40,0,1);
	path = dmatrix(0,9,0,1);
	fp = fopen("actpth.dat","r");
	for(i=0;i<40;i++)
		fscanf(fp,"%lf %lf\n",&actpth[i][0],&actpth[i][1]);
	fclose(fp);
	fp = fopen("path.dat","r");
	for(i=0;i<9;i++)
		fscanf(fp,"%lf %lf\n",&path[i][0],&path[i][1]);
	fclose(fp);
	start_pt[0] = 2.0;	start_pt[1] = 0.0;
	//test
	objfg5(nterm,9,start_pt,path,d_fcoeff,&d_L,&d_ang0);
	objfg5(nterm,40,start_pt,actpth,a_fcoeff,&a_L,&a_ang0);
	for(i=1;i<=nterm;i++)
	 printf("%lf  %lf\n",a_fcoeff[i][3],d_fcoeff[i][3]);



*/
	
//void objfg5(int nterm, int inc_no, double *start_pt, double **p_op_disp,double **fcoeff, double *d_L, double *d_ang)

objfg5(nterm,no_histpts,start_pt,spc_point,d_fcoeff,&d_L,&d_ang0);
objfg5(nterm,inc_no,start_pt,p_op_disp,a_fcoeff,&a_L,&a_ang0);



	  /* --- compute obj comps and their grads --- */
Atol = 0.0;
err_a = 0.0;
err_b = 0.0;
 for(i=1; i<=nterm; i++)
    Atol+= fabs(a_fcoeff[i][3]);
  Atol*= ARTOL/nterm; /* abs tolerance on A for corr alfa to be significant */
  for(i=1; i<=nterm; i++)
    {
      /* obj */
      dA= a_fcoeff[i][3] - d_fcoeff[i][3];
      err_A+= DSQR(dA);
	  dA= a_fcoeff[i][1] - d_fcoeff[i][1];
      err_a+= DSQR(dA);
	  dA= a_fcoeff[i][2] - d_fcoeff[i][2];
      err_b+= DSQR(dA);
	  
     if(fabs(a_fcoeff[i][3]) > Atol) 
	{
	  dalfa= a_fcoeff[i][4] - d_fcoeff[i][4];
	  err_alfa+= DSQR(dalfa); /* alfa imp iff A is sig. */
	}     
 
    }
 // err_L= DSQR(a_L - d_L);
  err_L = sqrt(pow((a_L-d_L),2));
  err_ang0= sqrt(DSQR(a_ang0 - d_ang0));
  err_A = sqrt(err_A);
  err_alfa = sqrt(err_alfa);
  err_a = sqrt(err_a);
  err_b = sqrt(err_b);
//printf("\n err_a = %lf err_b = %lf  err_L = %lf   err_ang0 = %lf",err_a,err_b,err_L,err_ang0);

  /* assemble obj and its grad */
// *obj= c_A*err_A +c_alfa*err_alfa + c_L*err_L + c_ang0*err_ang0;
// printf("\n %lf\n",*obj);
 *obj = c_a*err_a + c_b*err_b + c_L*err_L + c_ang0*err_ang0;
// *obj = (err_a + 1.0)*(1.0 + err_b)*(1.0 + err_L)*(1.0 + err_ang0);
 err_frame[0] = err_a;
 err_frame[1] = err_b;
 err_frame[2] = err_L;
 err_frame[3] = err_ang0;
 free_dmatrix(d_fcoeff,1, nterm, 1, 4);
 free_dmatrix(a_fcoeff,1, nterm, 1, 4);

}