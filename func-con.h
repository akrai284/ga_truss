# include "meshupdate_curved.h"
# include "frame_nonlinear_obj_curved_new.h"


/*This is the program used to evaluate the value of the function
************************************************************************/


void evaluate_func(int nvar, int nchrom, int popsize, double **population,double *fittness,
				   int obj_opt,double **error_values,double *error_coeff,double *start_point,
				   int n_segment)
{
  	int i,j,break_status=0; 
	
	double *x,f,err_func[6];

	
	int** elem, ** dispbc, ** forces1, ** forces2;
	double** node, * F1data, ** F2data, *TF1data, **TF2data;
	int Nelem, Nnode, Ndispbc1, Nforces1, Nforces2, NINCR, no_histpts;

	int** Telem, ** Tdispbc, ** Tforces1, ** Tforces2, ** int_con;
	double** Tnode, ** Tx, * L_calculated, **path_points;
	int TNelem, TNnode, TNdispbc1, TNforces1, TNforces2;
	double E0, ES, ET, thick;

	int** old_Telem;
	double** old_Tnode;
	int old_TNelem, old_TNnode;

	int a = read_nodeelem(Nnode, Nelem, Ndispbc1, Nforces1, Nforces2, E0, ES, ET,
		NINCR, thick, no_histpts);

	x = dvector(0, nvar + nchrom - 1);
	/*Allocate all vector here*/
	node = dmatrix(0, Nnode - 1, 0, 2);
	elem = imatrix(0, Nelem - 1, 0, 2);
	dispbc = imatrix(0, Ndispbc1 - 1, 0, 2);
	forces1 = imatrix(0, Nforces1 - 1, 0, 2);
	F1data = dvector(0, Nforces1 - 1);
	forces2 = imatrix(0, Nforces2 - 1, 0, 2);
	F2data = dmatrix(0, Nforces2 - 1, 0, 1);

	Tnode = dmatrix(0, Nnode - 1, 0, 2);
	Telem = imatrix(0, Nelem - 1, 0, 2);
	old_Tnode = dmatrix(0, Nnode - 1, 0, 2);
	old_Telem = imatrix(0, Nelem - 1, 0, 2);
	Tdispbc = imatrix(0, Ndispbc1 - 1, 0, 2);
	Tforces1 = imatrix(0, Nforces1 - 1, 0, 4);
	Tx = dmatrix(0, Nelem * n_segment - 1, 0, 3);
	int_con = imatrix(0, Nelem - 1, 0, 2);
	TF1data = dvector(0, Nforces1 - 1);
	TF2data = dmatrix(0, Nforces2 - 1, 0, 1);
	Tforces2 = imatrix(0, Nforces2 - 1, 0, 4);
	L_calculated = dvector(0, Nelem - 1);
        path_points = dmatrix(0, no_histpts, 0, 1);

	a = read_datafile(node, elem, dispbc, forces1, forces2, F1data, F2data,
		Nnode, Nelem, Ndispbc1, Nforces1, Nforces2,no_histpts, path_points);

	
	for(i = 0;i < popsize;i++)
	{
		for(j = 0; j < nvar+nchrom; j++)
			x[j] = population[i][j];
		for(j=0;j<4;j++)
			err_func[j] = 10000.0;
//	  printf("(start)pop number : %d  ",i+1);
   
	// Fittness functions
//	  printf("entering in mesh update \n");
//	meshupdate_curved(x,&break_status,nvar,nchrom,obj_opt,n_segment);
//	printf("out of mesh update \n");
		meshupdate_curved(x, &break_status, nvar, nchrom, obj_opt, n_segment,
			Telem, Tnode, Tdispbc, Tforces1, Tforces2, Tx, int_con, L_calculated,
			TNelem, TNnode, TNdispbc1, TNforces1, TNforces2,
			old_Tnode, old_Telem, old_TNnode, old_TNelem,
			node, elem, dispbc, forces1, forces2,
			F1data, F2data, TF1data, TF2data, Nnode, Nelem, Ndispbc1, Nforces1, Nforces2,
			E0, ES, ET, NINCR, thick, no_histpts);

		if ((TNdispbc1 == 0) || (TNforces1 != Nforces1) || (TNforces2 != Nforces2))
		{
			break_status = 1;
			

		}

		/*
		write_datafile("debug", Tnode, Telem, Tdispbc, Tforces1, Tforces2, F1data, F2data, TNnode, TNelem, TNdispbc1,
			TNforces1, TNforces2, E0, ES, ET, NINCR, thick, no_histpts);
		FILE* fp;
		fp = fopen("debugvar.dat", "w+");
		for (int ii = 0; ii < nvar + nchrom; ii++)
			fprintf(fp, "%lf ", x[ii]);
		fclose(fp);
		*/
	if(break_status==1)	
	{
		f = 100001.0;
//		printf("\n in mesh update %10.4lf",f);
	}			
	else
	{
//		printf("in frame");
//		frame_nonlinear_obj_curved(&f,obj_opt,err_func,error_coeff,start_point,n_segment);
		frame_nonlinear_obj_curved(&f, obj_opt, err_func, error_coeff, start_point, n_segment
			, Tnode, Telem, Tdispbc, Tforces1, Tforces2, TNnode, TNelem, TNdispbc1, TNforces1, TNforces2,
			int_con, TF1data, TF2data, Tx, old_Tnode, old_Telem, old_TNnode, old_TNelem, E0, ES, ET, NINCR, no_histpts,
			thick, L_calculated, path_points);
//		printf("out frame \n");
	}

	if (isnan(f)) {
		f = 100004;
	}
	fittness[i] = -f;
/*	if(obj_opt == 1)
		printf("fourier obj    OBJ[%2d] = %10.4lf\n",i+1,f);
	else
		printf("minmax obj    OBJ[%2d] = %10.4lf\n",i+1,f);
*/
	for(j=0;j<4;j++)
		error_values[i][j] = err_func[j];
//	printf("End\n");
	}

	free_dvector(x,0,nvar+nchrom-1);
	free_dmatrix(node,0, Nnode - 1, 0, 2);
	free_imatrix(elem,0, Nelem - 1, 0, 2);
	free_imatrix(dispbc,0, Ndispbc1 - 1, 0, 2);
	free_imatrix(forces1,0, Nforces1 - 1, 0, 2);
	free_dvector(F1data,0, Nforces1 - 1);
	free_imatrix(forces2,0, Nforces2 - 1, 0, 2);
	free_dmatrix(F2data,0, Nforces2 - 1, 0, 1);

	free_dvector(TF1data,0, Nforces1 - 1);
	free_dmatrix(TF2data,0, Nforces2 - 1, 0, 1);

	free_dmatrix(Tnode,0, Nnode - 1, 0, 2);
	free_imatrix(Telem,0, Nelem - 1, 0, 2);
	free_dmatrix(old_Tnode,0, Nnode - 1, 0, 2);
	free_imatrix(old_Telem,0, Nelem - 1, 0, 2);
	free_imatrix(Tdispbc,0, Ndispbc1 - 1, 0, 2);
	free_imatrix(Tforces1,0, Nforces1 - 1, 0, 4);
	free_dmatrix(Tx,0, Nelem * n_segment - 1, 0, 3);
	free_imatrix(int_con,0, Nelem - 1, 0, 2);
	free_imatrix(Tforces2,0, Nforces2 - 1, 0, 4);
	free_dvector(L_calculated,0, Nelem - 1);
	return;
}






