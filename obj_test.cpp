#include <iostream>
#include <stdio.h>
#include <math.h>
#include <time.h>
#include "nrutil.h"
//#include "ignore_comment.h"

#include "frame_nonlinear_obj_curved_new.h"
#include "meshupdate_curved.h"
int main()
{
	int i,j,k,obj_opt;
	double *x;

	double OBJ[2],err_frame[4],error_coeff[4],start_point[2];
	int nvar,nchrom, break_status = 0,n_segment=2;

	FILE *fp;
	


	fp = fopen("inputga.dat","r");
	ignore_comment(fp);	
	fscanf(fp,"%d %d",&nvar,&nchrom);
	
	ignore_comment(fp);	
	fscanf(fp,"%d ",&obj_opt);
	for(i=0;i<15;i++)
		ignore_comment(fp);
	fscanf(fp,"%d\n",&n_segment);
	if(obj_opt == 1)
	{
		ignore_comment(fp);
		fscanf(fp,"%lf %lf %lf %lf",&error_coeff[0],&error_coeff[1],&error_coeff[2],&error_coeff[3]);
		ignore_comment(fp);
		fscanf(fp,"%lf %lf ",&start_point[0],&start_point[1]);
	}

	printf("nvar: %3d, nchrom: %3d obj_opt = %3d\n",nvar,nchrom,obj_opt);
	fclose(fp);


	x = dvector(0,nvar+nchrom-1);
	fp = fopen("var.dat","r");
	for(i=0;i<nvar+nchrom;i++)
	{
		fscanf(fp,"%lf",&x[i]);//	printf("%3.4f \n",x[i]);
	}
	fclose(fp);


	double ti;
	time_t start,end;
	time(&start);
	
	int** elem, ** dispbc, ** forces1, ** forces2;
	double** node, *F1data, **F2data, *TF1data, **TF2data;
	int Nelem, Nnode, Ndispbc1, Nforces1, Nforces2, NINCR, no_histpts;

	int** Telem, ** Tdispbc, ** Tforces1, ** Tforces2, **int_con;
	double** Tnode,**Tx, *L_calculated, **path_points;
	int TNelem, TNnode, TNdispbc1, TNforces1, TNforces2;
	double E0, ES, ET, thick;

	int** old_Telem;
	double** old_Tnode;
	int old_TNelem, old_TNnode;


	int a = read_nodeelem(Nnode, Nelem, Ndispbc1, Nforces1, Nforces2, E0, ES, ET,
		NINCR, thick, no_histpts);

	/*Allocate all vector here*/
	node = dmatrix(0, Nnode - 1, 0, 2);
	elem = imatrix(0, Nelem - 1, 0, 2);
	dispbc = imatrix(0, Ndispbc1 - 1, 0, 2);
	forces1 = imatrix(0, Nforces1 - 1, 0, 2); 
	F1data = dvector(0, Nforces1 - 1);
	forces2 = imatrix(0, Nforces2 - 1, 0, 2); 
	F2data = dmatrix(0, Nforces2 - 1, 0, 1);

	Tnode = dmatrix(0, Nnode - 1, 0, 2);
	Telem = imatrix(0, Nelem - 1, 0, 2);
	old_Tnode = dmatrix(0, Nnode - 1, 0, 2);
	old_Telem = imatrix(0, Nelem - 1, 0, 2);
	Tdispbc = imatrix(0, Ndispbc1 - 1, 0, 2);
	Tforces1 = imatrix(0, Nforces1 - 1, 0, 4);
	Tx = dmatrix(0, Nelem*n_segment - 1, 0, 3);
	int_con = imatrix(0, Nelem - 1, 0, 2);
	TF1data = dvector(0, Nforces1 - 1);
	TF2data = dmatrix(0, Nforces2 - 1, 0, 1);
	Tforces2 = imatrix(0, Nforces2 - 1, 0, 4);
	L_calculated = dvector(0, Nelem - 1);
    path_points = dmatrix(0, no_histpts, 0, 1);
        
	a = read_datafile(node, elem, dispbc, forces1, forces2, F1data, F2data,
		Nnode, Nelem, Ndispbc1, Nforces1,Nforces2,no_histpts, path_points);

	meshupdate_curved(x, &break_status, nvar, nchrom, obj_opt, n_segment,
		Telem, Tnode, Tdispbc, Tforces1, Tforces2,Tx,int_con,L_calculated,
		TNelem, TNnode, TNdispbc1, TNforces1, TNforces2,
		old_Tnode, old_Telem, old_TNnode, old_TNelem,
		node,elem,dispbc,forces1,forces2,
		F1data,F2data,TF1data, TF2data, Nnode,Nelem,Ndispbc1,Nforces1,Nforces2,
		E0,ES,ET,NINCR,thick,no_histpts);
	
	printf("%d, %d \n", TNelem, TNnode);
	
	write_datafile("TT", Tnode, Telem, Tdispbc, Tforces1, Tforces2, F1data, F2data, TNnode, TNelem
		, TNdispbc1, TNforces1, TNforces2, E0, ES, ET, NINCR, thick, no_histpts);	

	printf("\n break status in mesh update = %d\n",break_status);
	time(&end);
	ti = difftime(end,start);
	printf("\n time in mesh update= %lf\n",ti);

	time(&start);
	frame_nonlinear_obj_curved(OBJ,obj_opt,err_frame,error_coeff,start_point,n_segment
	,Tnode,Telem,Tdispbc, Tforces1, Tforces2,TNnode,TNelem,TNdispbc1,TNforces1,TNforces2,
		int_con,TF1data,TF2data,Tx,old_Tnode,old_Telem,old_TNnode, old_TNelem,E0,ES,ET,NINCR,no_histpts,
		thick, L_calculated, path_points);
	{	 printf("OBJ[0]: %6.5lf \n",OBJ[0]); }
	
	
	time(&end);
	ti = difftime(end,start);
	
	
	printf("\n time in frame= %lf\n",ti);
	free_dvector(x,0,nvar+nchrom-1);
	free_dmatrix(node, 0, Nnode - 1, 0, 2);
	free_imatrix(elem, 0, Nelem - 1, 0, 2);
	free_imatrix(dispbc, 0, Ndispbc1 - 1, 0, 2);
	free_imatrix(forces1, 0, Nforces1 - 1, 0, 2);
	free_dvector(F1data, 0, Nforces1 - 1);
	free_imatrix(forces2, 0, Nforces2 - 1, 0, 2);
	free_dmatrix(F2data, 0, Nforces2 - 1, 0, 1);

	free_dmatrix(Tnode, 0, Nnode - 1, 0, 2);
	free_imatrix(Telem, 0, Nelem - 1, 0, 2);
	free_dmatrix(old_Tnode, 0, Nnode - 1, 0, 2);
	free_imatrix(old_Telem, 0, Nelem - 1, 0, 2);
	free_imatrix(Tdispbc, 0, Ndispbc1 - 1, 0, 2);
	free_imatrix(Tforces1, 0, Nforces1 - 1, 0, 4);
	free_dmatrix(Tx, 0, Nelem * n_segment - 1, 0, 3);
	free_imatrix(int_con, 0, Nelem - 1, 0, 2);
	free_imatrix(Tforces2, 0, Nforces2 - 1, 0, 4);
	free_dvector(L_calculated, 0, Nelem - 1);

	getchar();
        return(0);

}