#include<stdio.h>
#include<math.h>
#include<conio.h>
#include "nrutil.h"
#include "intersect_chk.h"
void ignore_comment(FILE *fp)
{
 char ss;
   do{ fscanf(fp,"%c",&ss);
   // printf("%c",ss);
   }while(ss!=':');
}

void main()
{
	int i,j,k;
	double *nx,*ny,**ZZp,dummy;
	int **elem,Nelem,break_status,Nnode;
	FILE *fp;
	fp = fopen("Tnodeelem.dat","r");
	ignore_comment(fp);
	fscanf(fp,"%d",&Nnode);
	ignore_comment(fp);
	fscanf(fp,"%d",&Nelem);
	fclose(fp);
	elem = imatrix(0,Nelem-1,0,2);
	fp = fopen("Telem.dat","r");
	i=0;
	while(i<Nelem)
	{
		fscanf(fp,"%d %d %d %lf",&elem[i][0],&elem[i][1],&elem[i][2],&dummy);		
		i++;
	}
	fclose(fp);
	nx = dvector(0,Nnode-1);
	ny = dvector(0,Nnode-1);
	fp = fopen("Tnode.dat","r");
	for(i=0;i<Nnode;i++)
		fscanf(fp,"%lf %lf %lf\n",&dummy,&nx[i],&ny[i]);
	fclose(fp);

	ZZp = dmatrix(0,Nelem-1,0,1);
	fp = fopen("Tx.dat","r");
	for(i=0;i<Nelem;i++)
		fscanf(fp,"%lf %lf %lf %lf\n",&dummy,&dummy,&ZZp[i][0],&ZZp[i][1]);
	fclose(fp);
	for(i=0;i<Nelem;i++)
	{
		j = elem[i][1];	k = elem[i][2];
		dummy = atan2((ny[k-1] - ny[j-1]),(nx[k-1] - nx[j-1]));
		ZZp[i][0]+=dummy;
		ZZp[i][1]+=dummy;
	}
/*	elem[0][0] = 1;	elem[0][1] = 1;	elem[0][2] = 2;
	elem[0][0] = 1;	elem[0][0] = 1;	elem[0][0] = 3;
	nx[0] = 0.0;	nx[1] = 1.0;	nx[2] = 1.0;
	ny[0] = 0.0;	ny[1] = 1.0;	ny[2] = -1.0;
	ZZp[0][0] = 0.0854;	ZZp[0][1] = 1.4524;
	ZZp[1][0] = -0.0845;	ZZp[1][1] = 1.4524;
	Nelem = 2;
*/
	intersection_chk(elem,Nelem,nx,ny,ZZp,&break_status);

}