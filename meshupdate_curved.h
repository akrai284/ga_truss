// # include "nrutil.h"
#include "sorting.h"
//int nvar, nchrom;

void ignore_comment(FILE *fp);
void compute_node(double **mid_node,double *mid_slope,double **end_node,
				  double *end_slope,int n_segment);

void new_freeblock(int **Telem, int TNelem,int *forcenode,int *fc_n1,int *extra_elem,
				   int *extra_elem_n);
void dang_element(int **Telem, int TNelem,int *forcenode,int fc_n,int *dang_elem, int *dang_elem_n);

int dof_check(int **elem,int Nelem,int Nnode,int *fix_node,int Nfix);

int read_nodeelem(int& Nnode, int& Nelem, int& Ndispbc1, int& Nforces1, int& Nforces2, double& E0, double& ES, double& ET,
	int& NINCR, double& thick, int& no_histpts)
{
	FILE* fpi, * fpo;
	int print_opt = 0;
	int i, j, k, ppp;
	fpi = fopen("nodeelem.dat", "r");

	if (fpi != NULL) {
		ignore_comment(fpi);				fscanf(fpi, "%d\n", &Nnode); 			if (print_opt == 1) { printf("%4d\n", Nnode); }
		ignore_comment(fpi);				fscanf(fpi, "%d\n", &Nelem); 			if (print_opt == 1) { printf("%4d\n", Nelem); }
		ignore_comment(fpi);				fscanf(fpi, "%d\n", &Ndispbc1);		if (print_opt == 1) { printf("%4d\n", Ndispbc1); }
		ignore_comment(fpi);				fscanf(fpi, "%d\n", &Nforces1);		if (print_opt == 1) { printf("%4d\n", Nforces1); }
		ignore_comment(fpi);				fscanf(fpi, "%d\n", &Nforces2);		if (print_opt == 1) { printf("%4d\n", Nforces2); }
		ignore_comment(fpi);				fscanf(fpi, "%le\n", &thick);			if (print_opt == 1) { printf("%6.5f\n", thick); }
		ignore_comment(fpi);				fscanf(fpi, "%d %le\n", &i, &E0);			if (print_opt == 1) { printf("%6.5f\n", E0); }
		ignore_comment(fpi);				fscanf(fpi, "%d %le\n", &i, &ES);			if (print_opt == 1) { printf("%6.5f\n", E0); }
		ignore_comment(fpi);				fscanf(fpi, "%d %le\n", &i, &ET);			if (print_opt == 1) { printf("%6.5f\n", E0); }
		ignore_comment(fpi);				fscanf(fpi, "%d\n", &no_histpts); 	if (print_opt == 1) { printf("%4d\n", no_histpts); }
		ignore_comment(fpi);				fscanf(fpi, "%d\n", &NINCR); 			if (print_opt == 1) { printf("%4d\n", NINCR); }
		fclose(fpi);

	}
	else
	{
		printf("error reading nodeelem.dat \n");
		return(-1);
	}
        return(0);
}
int write_datafile(string fileprefix, double** node, int** elem, int** dispbc, int** forces1, int** forces2,
	double* F1data, double** F2data,
	int Nnode, int Nelem, int Ndispbc1, int Nforces1, int Nforces2,
	double E0, double ES, double ET,
	int NINCR, double thick, int no_histpts)

{
	FILE* fpi;
	int print_opt = 0;
	int i, j, k, ppp;

	string filename;
	
	filename = fileprefix + "nodeelem.dat";
	fpi = fopen(filename.c_str(), "w");		
	fprintf(fpi, "number of nodes: %3d\n", Nnode);
	fprintf(fpi, "number of elements: %3d\n", Nelem);
	fprintf(fpi, "number of displacement boundary conditions: %3d\n", Ndispbc1);
	fprintf(fpi, "number of input forces: %3d\n", Nforces1);
	fprintf(fpi, "number of output displacements: %3d\n", Nforces2);
	fprintf(fpi, "thick: %6.5f\n", thick);
	fprintf(fpi, "E0: %6.5f\n", E0);
	fprintf(fpi, "ES: %6.5f\n", ES);
	fprintf(fpi, "ET: %6.5f\n", ET);
	fprintf(fpi, "number of history points: %3d \n", no_histpts);
	fprintf(fpi, "number of increment : %3d \n", NINCR);
	fclose(fpi);	

	filename = fileprefix + "node.dat";
	fpi = fopen(filename.c_str(), "w");
	if (fpi != NULL)
	{
		
		for (i = 0; i < Nnode; i++) { 
			fprintf(fpi, "%4d %8.5lf %8.5lf\n", node[i][0], node[i][1], node[i][2]); 
		}
		fclose(fpi);
	}
	else { printf("error writing node.dat \n");		return(-1); }


	if (print_opt == 1)
		for (i = 0; i < Nnode; i++)
		{
			printf("i: %6.5f,node_x: %6.5f, node_y: %6.5f \n", node[i][0], node[i][1], node[i][2]);
		}





	filename = fileprefix + "elem.dat";
	fpi = fopen(filename.c_str(), "w");
	if (fpi != NULL)
	{
		for (i = 0; i < Nelem; i++) { fprintf(fpi, "%d %d %d %lf\n", elem[i][0], elem[i][1], elem[i][2], E0); }
		fclose(fpi);
	}
	else { printf("error reading elem.dat \n");		return(-1); }

	filename = fileprefix + "dispbc1.dat";
	fpi = fopen(filename.c_str(), "w");
	if (fpi != NULL)
	{
		for (i = 0; i < Ndispbc1; i++) { fprintf(fpi, "%d %d %d \n", dispbc[i][0], dispbc[i][1], dispbc[i][2]); }
		fclose(fpi);
	}
	else { printf("error reading dispbc1.dat \n");		return(-1); }


	filename = fileprefix + "forces1.dat";
	fpi = fopen(filename.c_str(), "w");
	if (fpi != NULL)
	{
		for (i = 0; i < Nforces1; i++)
		{
			fprintf(fpi, "%d %d %d %lf\n", forces1[i][0], forces1[i][1], forces1[i][2], F1data[i]);
		}
		fclose(fpi);
	}
	else { printf("error reading forces1.dat \n");		return(-1); }


	filename = fileprefix + "forces2.dat";
	fpi = fopen(filename.c_str(), "w");
	if (fpi != NULL)
	{
		for (i = 0; i < Nforces2; i++)
		{
			fprintf(fpi, "%d %d %d %lf %lf\n", forces2[i][0], forces2[i][1], forces2[i][2], F2data[i][0], F2data[i][1]);
		}
		fclose(fpi);
	}
	else { printf("error reading forces2.dat \n");		return(-1); }

	return(0);

}

int read_datafile(double** node, int** elem, int** dispbc, int** forces1, int** forces2,
	double *F1data, double** F2data,
	int Nnode, int Nelem, int Ndispbc1, int Nforces1, int Nforces2, int Npathpoints , double **path_points)
	
{
	FILE* fpi, * fpo;
	int print_opt = 0;
	int i, j, k;
	double ppp;
	
	
	fpi = fopen("node.dat", "r");
	if (fpi != NULL)
	{
		for (i = 0; i < Nnode; i++) { fscanf(fpi, "%lf %lf %lf\n", &node[i][0], &node[i][1], &node[i][2]); }
		fclose(fpi);		
	}
	else { printf("error reading node.dat \n");		return(-1); }


	if (print_opt == 1)
		for (i = 0; i < Nnode; i++)
		{
			printf("i: %6.5f,node_x: %6.5f, node_y: %6.5f \n", node[i][0], node[i][1], node[i][2]);
		}




	
	fpi = fopen("elem.dat", "r");		
	if (fpi != NULL)
	{
		for (i = 0; i < Nelem; i++) { fscanf(fpi, "%d %d %d %lf\n", &elem[i][0], &elem[i][1], &elem[i][2], &ppp); }
		fclose(fpi);		
	}
	else { printf("error reading elem.dat \n");		return(-1); }
	
	fpi = fopen("dispbc1.dat", "r");			
	if (fpi != NULL)
	{
		for (i = 0; i < Ndispbc1; i++) { fscanf(fpi, "%d %d %d \n", &dispbc[i][0], &dispbc[i][1], &dispbc[i][2]); }
		fclose(fpi);	
	}
	else { printf("error reading dispbc1.dat \n");		return(-1); }

	
	fpi = fopen("forces1.dat", "r");		
	if (fpi != NULL)
	{
		for (i = 0; i < Nforces1; i++)
		{
			fscanf(fpi, "%d %d %d %lf\n", &forces1[i][0], &forces1[i][1], &forces1[i][2], &F1data[i]);
		}
		fclose(fpi);		
	}
	else { printf("error reading forces1.dat \n");		return(-1); }

	
	fpi = fopen("forces2.dat", "r");			
	if (fpi != NULL)
	{
		for (i = 0; i < Nforces2; i++)
		{
			fscanf(fpi, "%d %d %d %lf %lf\n", &forces2[i][0], &forces2[i][1], &forces2[i][2], &F2data[i][0], &F2data[i][1]);
		}
		fclose(fpi);		
	}
	else { printf("error reading forces2.dat \n");		return(-1); }
        
        fpi = fopen("histspc.dat","r");	
		 if (fpi != NULL)
		 {
			 for(i=0;i<Npathpoints;i++)
			 {
				fscanf(fpi,"%d %lf %lf",&ppp,&path_points[i+1][0],&path_points[i+1][1]);
			 }
			 fclose(fpi);
		 }
		 else
		 {
			 printf("error reading histspc.dat \n"); getchar();
		 }

		 path_points[0][0] = 0.0;	path_points[0][1] = 0.0;

	return(0);

}

void meshupdate_curved(double *X, int *break_status,int nvar, int nchrom, int obj_opt,
					   int n_segment,
		int** new_con, double **new_node, int ** new_dispbc, int** new_forcebc1, int** new_forcebc2,
		double **new_tx, int **int_con, double *L_calculated,
	int &final_element, int &final_node, int &TNdispbc1, int &TNforces1, int &TNforces2,
	double ** Tnode, int ** Telem,int &TNnode, int &TNelem,
	double** node, int** elem, int** dispbc, int** forces1, int** forces2,
	double* F1data, double** F2data, double *TF1data, double **TF2data, int Nnode, int Nelem, int Ndispbc1, int Nforces1, 
	int Nforces2, double E0, double ES, double ET,
	int NINCR, double &thick, int no_histpts
	)
{

	
	int file_count_open = 0,file_count_close = 0;

	double ppp,max_length;
   
	double **tempTnode,**Tx,d_o;
    int **tempTelem,*xx, print_opt = 0;
	


	int node_v,width_v,thick_v,curvature_v,force_v,beamtruss_v;
	int entry_stat,x_entry_stat,y_entry_stat;
	double **mid_node,**end_node,*mid_slope,*end_slope;
	int check_truss =1;

	int i,j,k,eye,jay;

	int update_fix_coordinate = 1;
	int update_force1_coordinate = 1;
	int update_force2_coordinate = 0;


	FILE *fpi, *fpo;

	d_o = 0.01;
	  
	if(print_opt==1)		{	printf("nvar and nchrom in meshupdate: %3d, %3d\n",nvar,nchrom); getchar();	}

	


	
	xx = ivector(0,Nelem-1);
/*****************************************************/
	node_v = 2*Nnode;					//end position of all variables in X
	width_v = node_v + Nelem*n_segment;	//node_v to width_v		width_v -> inplane width
	thick_v = 1 + width_v;				//width_v to thick_v	thick_v -> along Z-direction
	curvature_v =thick_v + 2*Nelem;		//thick_v to curvature_v
	force_v = curvature_v + Nforces1;	//curvature_v to force_v
	beamtruss_v = force_v + nchrom;
/*********************************************************/
	
	*break_status=0;
	//printf("in mesh update file reading completed\n");

	max_length = 2.0*pow(((node[elem[0][1]-1][1] - node[elem[0][2]-1][1])*(node[elem[0][1]-1][1] - node[elem[0][2]-1][1])
		+ (node[elem[0][1]-1][2] - node[elem[0][2]-1][2])*(node[elem[0][1]-1][2] - node[elem[0][2]-1][2])),0.5);
	for(i=0;i<nchrom;i++)
	{
		if(X[force_v + i]<=2 || X[force_v + i]>=4)
			xx[i] = 1;
		else if(X[force_v + i]>2 && X[force_v + i]<=3)
			xx[i] = 2;
		else if(X[force_v + i]>3 && X[force_v + i]<4)
			xx[i] = 3;
		
	}
   	// SET THE CURVATURES BEFORE CHANGING THE NODAL COORDINATES
	// compute slopes of elements
	

		// find if some nodes are fixed, if yes, don't change the curvature...

		for(i=0;i<nchrom;i++)
		{	
			entry_stat = 0; x_entry_stat=0; y_entry_stat=0;
			eye = elem[i][1];			jay = elem[i][2];

			L_calculated[i] = max_length/8.0 + 7.0/8.0*max_length*(fabs(X[thick_v+2*i]))/(fabs(X[thick_v+2*i]) +
				fabs(X[thick_v+2*i+1]));

			for(j=0;j<Ndispbc1;j++)				// find if a node has an entry in dbcs.
			{	
				if(dispbc[j][1]==eye)			// first entry is in dbc
				{	
					 X[thick_v+2*i] = 0;	   // curvature at node 1
					 break;
				}

				if(dispbc[j][1]==jay)			// second entry is in dbc
				{	
					  X[thick_v+2*i + 1] = 0;	   // curvature at node 2					
					  break;
				}

			}

			if(xx[i] >= 2)
			{
				X[thick_v+2*i] = 0;
				X[thick_v+2*i+1] = 0;
			}

		}

		//getchar();


	// NOW update the nodal coordinates from X
	// make sure that the coordinates for fixed nodes, input force and output displacement DO NOT CHANGE
	for(i=0;i<Nnode;i++)
	{	
		entry_stat = 0; x_entry_stat=0; y_entry_stat=0;
		
		if(update_fix_coordinate != 1)
		{
			for(j=0;j<Ndispbc1;j++)				// find if a node has an entry in dbcs.
			{	
				if(dispbc[j][1]==i+1)			// node i+1 has an entry
				{	
					entry_stat = 1;
					if(dispbc[j][2]==1)			{ x_entry_stat = 1;	}	
					if(dispbc[j][2]==2)			{ y_entry_stat = 1;	}
				}
			}
		}

		if(update_force1_coordinate != 1)
		{
			for(j=0;j<Nforces1;j++)				// find if a node has an entry as input port.
			{	
				if(forces1[j][1]==i+1)			// node i+1 has an entry
				{	
					entry_stat = 1;
					if(forces1[j][2]==1)			{ x_entry_stat = 1;	}	
					if(forces1[j][2]==2)			{ y_entry_stat = 1;	}
				}
			}
		}

		if(update_force2_coordinate != 1)
		{
			for(j=0;j<Nforces2;j++)				// find if a node has an entry as input port.
			{	
				if(forces2[j][1]==i+1)			// node i+1 has an entry
				{	
					entry_stat = 1;
					if(forces2[j][2]==1)			{ x_entry_stat = 1;	}	
					if(forces2[j][2]==2)			{ y_entry_stat = 1;	}
				}
			}
		}


		if(print_opt==1)	{	printf("ent-stat: %1d, x_stat: %1d, y_stat: %1d \n",entry_stat,x_entry_stat,y_entry_stat);	}

		if(entry_stat!=1)					// node i+1 is not fixed, update nodal coordinates
		{	node[i][1] = X[2*i];	node[i][2] = X[2*i+1];
		}
		else								// node i+1 is fixed, eith x or y or both dofs are fixed
		{	
			if((x_entry_stat==1)&&(y_entry_stat!=1))		// only x is fixed, so update the y coordinate
			{	node[i][2] = X[2*i+1];
			}
			if((x_entry_stat!=1)&&(y_entry_stat==1))	// only y is fixed, so update the x coordinate
			{	node[i][1] = X[2*i];
			}
			// else if(x_entry_stat==1)&&(y_entry_stat==1)	// both x and y are fixed, don't update the coordinates
		}
		if(print_opt==1)	{	printf("i: %6.5f,node_x: %6.5f, node_y: %6.5f \n",node[i][0],node[i][1],node[i][2]);	}
	}
	if(print_opt==1)		{	getchar();	}


	// updated value of thickness
	thick = X[width_v];
	if (print_opt==1)	{	printf("updated value of thickness: %6.5f \n",thick);	}


	// renumbering algorithm to update mesh after removing the non-existing elements
	// make sure to update/record the widths as well


	TNelem = 0;
	for(i=0;i<Nelem;i++)
	{
		if(X[nvar+i]>0)
			TNelem++;
	}
	
/*************************updating the check stress condition **************************/
	for(i=0;i<Nelem;i++)
	{
		if(xx[i] != 2)
		{
			check_truss = 0;
			break;
		}
	}
/***************************************************************************************/


	
	if(TNelem>0)
	{
		int fc_n=0,extra_n,dang_n;
		int *extra_elem, *forcenode,*dang_elem;
		tempTelem = imatrix(0,TNelem-1,0,2);
		extra_elem = ivector(0,TNelem-1);
		dang_elem = ivector(0,TNelem-1);
		
		forcenode = ivector(0,Nforces1+Nforces2+Ndispbc1-1);
		

		for(i=0;i<Nforces1;i++)
			forcenode[i] = forces1[i][1];

		for(i=0;i<Nforces2;i++)
			forcenode[i+Nforces1] = forces2[i][1];
		fc_n = Nforces1+Nforces2;

		TNelem = 0;
		for(i=0;i<Nelem;i++)
		{
			if(X[nvar+i]>0)
			{
				tempTelem[TNelem][0] = i+1;
				tempTelem[TNelem][1] = elem[i][1];
				tempTelem[TNelem][2] = elem[i][2];
				TNelem++;
			}
		}
		//printf("in mesh update in new free block\n");
		new_freeblock(tempTelem,TNelem,forcenode,&fc_n,extra_elem,&extra_n);
		//printf("in mesh update out free block\n");
		for(i=0;i<Ndispbc1;i++)
			forcenode[i+fc_n] = dispbc[i][1];
		fc_n = fc_n+Ndispbc1;
		//printf("in mesh update in dang elem\n");
		dang_element(tempTelem,TNelem,forcenode,fc_n,dang_elem,&dang_n);
		//printf("in mesh update out dang elem\n");

		for(i=0;i<extra_n;i++)
		{
			X[nvar+extra_elem[i]-1] = 0;
		}

		for(i=0;i<dang_n;i++)
		{
			X[nvar+dang_elem[i]-1] = 0;
		}
		free_imatrix(tempTelem,0,TNelem-1,0,2);
		free_ivector(extra_elem,0,TNelem-1);
		free_ivector(dang_elem,0,TNelem-1);

		free_ivector(forcenode,0,Nforces1+Nforces2+Ndispbc1-1);
	}
	else
	{
		*break_status = 1;
		return;
	}

	int *vect,*rvect;
	TNelem = 0;
	FILE *fpp;
	for(i=0;i<Nelem;i++)
	{
		if(X[nvar+i]>0.0)
		{
			TNelem++;
			if(xx[i]!=2)
				check_truss=0;
		}
	}
	if(TNelem>0)
	{
		int Total_disp_node,*disp_node;
		disp_node = ivector(0,Ndispbc1-1);
		Total_disp_node = Ndispbc1;
		
		
		for(i=0;i<Ndispbc1;i++)
			disp_node[i] = dispbc[i][1];
		remove_duplicate(disp_node,&Total_disp_node);

		Tx = dmatrix(0,TNelem-1,0,3);
		
		j=0;
		vect = ivector(0,2*TNelem-1);
		rvect = ivector(0,2*TNelem-1);
		k = 0;
		for(i=0;i<Nelem;i++)
		{
			if(X[nvar+i]>0)
			{
				vect[j] = elem[i][1];
				vect[j+1] = elem[i][2];
				j+=2;
				Tx[k][0] = xx[i];
				Tx[k][1] = X[node_v+i];
				Tx[k][2] = X[thick_v+2*i];
				Tx[k][3] = X[thick_v+2*i+1];
				k++;
			}
		}
		renumbering(vect,2*TNelem,rvect);
		j = 0;
		for(i=0;i<TNelem;i++)
		{
			Telem[i][0] = i+1;
			Telem[i][1] = rvect[j];
			Telem[i][2] = rvect[j+1];
			j+=2;
		}
		j = 2*TNelem;
		remove_duplicate(vect,&j);
		TNnode = j;
		for(i=0;i<j;i++)
		{
			Tnode[i][0] = i+1;
			Tnode[i][1] = node[vect[i]-1][1];
			Tnode[i][2] = node[vect[i]-1][2];
		}

		j = 2*TNelem;
		remove_duplicate(rvect,&j);
		//writing force
		
		k = 1;
		for(i=0;i<Nforces1;i++)
		{
			for(j=0;j<TNnode;j++)
			{
				if(vect[j] == forces1[i][1])
					break;
			}
			if(j<TNnode)
			{
				k++;
			}
		}
		TNforces1 = k-1;

		k = 1;
		for(i=0;i<Nforces2;i++)
		{	
			for(j=0;j<TNnode;j++)
			{
				if(vect[j] == forces2[i][1])
					break;
			}
			if(j<TNnode)
			{	
					k = k+1;			
			}
		}
		TNforces2 = k - 1;

		// generating displacement boundary condition info	
		k = 1;
		for(i=0;i<Ndispbc1;i++)
		{
			for(j=0;j<TNnode;j++)
			{
				if(vect[j] == dispbc[i][1])
					break;
			}
			if(j<TNnode)
			{
					k = k+1;			
			}
		}
		TNdispbc1 = k-1;

	
		int new_n, e;
		double th1, th2;

		final_element = TNelem*n_segment;
		final_node	= TNnode + TNelem*(n_segment-1);
	
		e = 0;
		for(i=0;i<TNnode;i++)
		{
			int_con[i][0] = 0;
			int_con[i][1] = 0;
		}
		new_n = 1;
		if(n_segment>1)
		{
			mid_node	= dmatrix(0,n_segment-2,0,1);
			mid_slope	= dvector(0,n_segment-2);
			end_node	= dmatrix(0,1,0,1);
			end_slope	= dvector(0,1);
			for(i=0;i<TNelem;i++)
			{
				eye = Telem[i][1];	jay = Telem[i][2];
				end_node[0][0] = Tnode[eye-1][1];	end_node[0][1] = Tnode[eye-1][2];
				end_node[1][0] = Tnode[jay-1][1];	end_node[1][1] = Tnode[jay-1][2];
				end_slope[0] = Tx[i][2];	end_slope[1] = Tx[i][3];
				compute_node(mid_node,mid_slope,end_node,end_slope,n_segment);
				th1 = atan2((end_node[1][1] - end_node[0][1]),(end_node[1][0] - end_node[0][0]));



				if(int_con[eye-1][1] == 0)
				{
					int_con[eye-1][1] = new_n;
					int_con[eye-1][0] = eye;
					new_con[e][0] = e+1;
					new_con[e][1] = new_n;
					new_con[e][2] = new_n+1;
					new_node[new_n][0] = new_n+1;
					new_node[new_n][1] = mid_node[0][0];
					new_node[new_n][2] = mid_node[0][1];

					th2 = atan2((mid_node[0][1] - end_node[0][1]),(mid_node[0][0] - end_node[0][0]));

					new_tx[e][2] = Tx[i][2] + th1 - th2;
					new_tx[e][3] = mid_slope[0] + th1 - th2;
					new_n++;
					e++;
				}
				else
				{
					new_con[e][0] = e+1;
					new_con[e][1] = int_con[eye-1][1];
					new_con[e][2] = new_n;
					new_node[new_n-1][0] = new_n;
					new_node[new_n-1][1] = mid_node[0][0];
					new_node[new_n-1][2] = mid_node[0][1];

					th2 = atan2((mid_node[0][1] - end_node[0][1]),(mid_node[0][0] - end_node[0][0]));

					new_tx[e][2] = Tx[i][2]+ th1 - th2;
					new_tx[e][3] = mid_slope[0]+ th1 - th2;
					e++;
				}


				for(j=1;j<n_segment-1;j++)
				{
					new_con[e][0] = e+1;
					new_con[e][1] = new_n;
					new_con[e][2] = new_n+1;
					new_node[new_n][0] = new_n+1;
					new_node[new_n][1] = mid_node[j][0];
					new_node[new_n][2] = mid_node[j][1];

					th2 = atan2((mid_node[j][1] - mid_node[j-1][1]),(mid_node[j][0] - mid_node[j-1][0]));

					new_tx[e][2] = mid_slope[j-1]+ th1 - th2;
					new_tx[e][3] = mid_slope[j]+ th1 - th2;
					new_n++;
					e++;
				}

				
				if(int_con[jay-1][1] == 0)
				{
					
					new_con[e][0] = e+1;
					new_con[e][1] = new_n;
					new_con[e][2] = new_n+1;
					new_n++;
					int_con[jay-1][1] = new_n;
					int_con[jay-1][0] = jay;

					th2 = atan2((end_node[1][1] - mid_node[n_segment-2][1]),(end_node[1][0] - mid_node[n_segment-2][0]));

					new_tx[e][2] = mid_slope[n_segment-2]+ th1 - th2;
					new_tx[e][3] = Tx[i][3]+ th1 - th2;
					e++;
					new_n++;
				}
				else
				{
					new_con[e][0] = e+1;
					new_con[e][1] = new_n++;
					new_con[e][2] = int_con[jay-1][1];

					th2 = atan2((end_node[1][1] - mid_node[n_segment-2][1]),(end_node[1][0] - mid_node[n_segment-2][0]));

					new_tx[e][2] = mid_slope[n_segment-2]+ th1 - th2;
					new_tx[e][3] = Tx[i][3]+ th1 - th2;
					e++;
				}
				

			}
			for(i=0;i<TNnode;i++)
			{
				new_node[int_con[i][1]-1][1] = Tnode[int_con[i][0]-1][1];
				new_node[int_con[i][1]-1][2] = Tnode[int_con[i][0]-1][2];
			}
			free_dmatrix(mid_node,0,n_segment-2,0,1);
			free_dvector(mid_slope,0,n_segment-2);
			free_dmatrix(end_node,0,1,0,1);
			free_dvector(end_slope,0,1);
		}
		else
		{
			for(i=0;i<TNelem;i++)
			{
				new_con[i][0] = Telem[i][0];
				new_con[i][1] = Telem[i][1];
				new_con[i][2] = Telem[i][2];

				new_tx[i][0] = Tx[i][0];
				new_tx[i][1] = Tx[i][1];
				new_tx[i][2] = Tx[i][2];
				new_tx[i][3] = Tx[i][3];
			}
			for(i=0;i<TNnode;i++)
			{
				new_node[i][0] = Tnode[i][0];
				new_node[i][1] = Tnode[i][1];
				new_node[i][2] = Tnode[i][2];
				int_con[i][0] = i+1;
				int_con[i][1] = i+1;
			}
		}

/****************if all element are truss elements**********************/
		int f_node =1,f_dir=0;

		for(j=0;j<TNnode;j++)
		{
			if(vect[j] == forces1[0][1])
			{
				f_node = int_con[rvect[j]-1][1];
				f_dir = forces1[0][2];
				break;
			}
		}
	if(f_dir == 3)
	{
		for(i=0;i<final_element;i++)
		{
			if(new_con[i][1] == f_node || new_con[i][2] == f_node)
			{
				if(new_con[i][1] == f_node)
					f_node = new_con[i][2];
				break;
			}
		}
	}

		if(check_truss == 1)
		{
			k = 0;
			for(i=0;i<Nelem;i++)
			{
				if(X[nvar+i]>0.0)
				{
					for(j=0;j<n_segment;j++)
					{
						new_tx[k][0] = xx[i];
						new_tx[k][1] = X[node_v+n_segment*i+j];
						k++;
					}
				}
			}
			k = 1;
			for(i=0;i<Nforces1;i++)
			{
				for(j=0;j<TNnode;j++)
				{
					if(vect[j] == forces1[i][1])
						break;
				}
				if(j<TNnode)
				{
	//				if(obj_opt==1)
						ppp = F1data[i]/fabs(F1data[i])*X[curvature_v+k-1];
	//				else
	//					ppp = F1data[i];
						new_forcebc1[k - 1][0] = k;
						new_forcebc1[k - 1][1] = int_con[rvect[j] - 1][1];
						new_forcebc1[k - 1][2] = forces1[i][2];
						TF1data[k - 1] = ppp;
						k++;
				}
			}
			
			k = 1;
			for(i=0;i<Nforces2;i++)
			{	
				for(j=0;j<TNnode;j++)
				{
					if(vect[j] == forces2[i][1])
						break;
				}
				if(j<TNnode)
				{	
					new_forcebc2[k - 1][0] = k;
					new_forcebc2[k - 1][1] = int_con[rvect[j] - 1][1];
					new_forcebc2[k - 1][2] = forces2[i][2];
					TF2data[k - 1][0] = F2data[i][0];
					TF2data[k - 1][1] = F2data[i][1];
					k = k+1;			
				}
			}
		
			// generating displacement boundary condition info	
			k = 1;
			for(i=0;i<Ndispbc1;i++)
			{
				for(j=0;j<TNnode;j++)
				{
					if(vect[j] == dispbc[i][1])
						break;
				}
				if(j<TNnode)
				{
					new_dispbc[k - 1][0] = k;
					new_dispbc[k - 1][1] = int_con[rvect[j]-1][1];
					new_dispbc[k - 1][2] = dispbc[i][2];
					k = k+1;			
				}
			}
	//********************checking degree of freedom********************************/

			int *new_disp_node;
			new_disp_node = ivector(0,Total_disp_node-1);
			for(i=0;i<Total_disp_node;i++)
			{
				for(j=0;j<TNnode;j++)
				{
					if(vect[j] == disp_node[i])
						break;
				}
				if(j<TNnode)
				{
					new_disp_node[i]=int_con[rvect[j]-1][1];
				}
			}

			i = dof_check(Telem,TNelem,TNnode,new_disp_node,Total_disp_node);

			if(i!=1)
				*break_status = 1;


			free_ivector(new_disp_node,0,Total_disp_node-1);
	//******************end of dof check**************************************/
		} //end of check_truss
		else
		{
			k = 0;
			for(i=0;i<Nelem;i++)
			{
				if(X[nvar+i]>0.0)
				{
					for(j=0;j<n_segment;j++)
					{
						new_tx[k][0] = xx[i];
						new_tx[k][1] = X[node_v+n_segment*i+j];
						k++;
					}
				}
			}
			k = 1;
			for(i=0;i<Nforces1;i++)
			{
				for(j=0;j<TNnode;j++)
				{
					if(vect[j] == forces1[i][1])
						break;
				}
				if(j<TNnode)
				{
	//				if(obj_opt==1)
						ppp = F1data[i]/fabs(F1data[i])*X[curvature_v+k-1];
	//				else
	//					ppp = F1data[i];
						new_forcebc1[k - 1][0] = k;
						new_forcebc1[k - 1][1] = int_con[rvect[j] - 1][1];
						new_forcebc1[k - 1][2] = forces1[i][2];
						TF1data[k - 1] = ppp;
						k++;
				}
			}
			
			k = 1;
			for(i=0;i<Nforces2;i++)
			{	
				for(j=0;j<TNnode;j++)
				{
					if(vect[j] == forces2[i][1])
						break;
				}
				if(j<TNnode)
				{	
					new_forcebc2[k - 1][0] = k;
					new_forcebc2[k - 1][1] = int_con[rvect[j] - 1][1];
					new_forcebc2[k - 1][2] = forces2[i][2];
					TF2data[k - 1][0] = F2data[i][0];
					TF2data[k - 1][1] = F2data[i][1];
					k = k+1;			
				}
			}
		
		
			// generating displacement boundary condition info	
			k = 1;
			for(i=0;i<Ndispbc1;i++)
			{
				for(j=0;j<TNnode;j++)
				{
					if(vect[j] == dispbc[i][1])
						break;
				}
				if(j<TNnode)
				{
					new_dispbc[k - 1][0] = k;
					new_dispbc[k - 1][1] = int_con[rvect[j] - 1][1];
					new_dispbc[k - 1][2] = dispbc[i][2];
					k = k+1;			
				}
			}
	
		}  //end of else check_truss


		j = 0;
		for(i=0;i<Nelem;i++)
		{
			if(X[nvar+i]>0.0)
			{
				if(xx[i] == 3)
				{
					//no change in L_calculated[i]);
				}
				else
				{

					max_length = pow(((new_node[new_con[j][1]-1][1] - new_node[new_con[j][2]-1][1])*
									(new_node[new_con[j][1]-1][1] - new_node[new_con[j][2]-1][1])
									+ (new_node[new_con[j][1]-1][2] - new_node[new_con[j][2]-1][2])*
									(new_node[new_con[j][1]-1][2] - new_node[new_con[j][2]-1][2])),0.5);
					L_calculated[i] = max_length;

				}
				j++;
			}
		}

		free_ivector(disp_node,0,Ndispbc1-1);
		

		free_ivector(xx,0,Nelem-1);
		free_dmatrix(Tx,0,TNelem-1,0,3);
		
		free_ivector(vect,0,2*TNelem-1);
		free_ivector(rvect,0,2*TNelem-1);
	
	}
	else
		*break_status = 1;
	
	
	
	
}	// end of meshupdate



void compute_node(double **mid_node,double *mid_slope,double **end_node,
				  double *end_slope,int n_segment)
{
	int i;
	double th,nx1,nx2,ny1,ny2,L,*t,temp1,temp2;
	t	=	dvector(0,n_segment-2);

	nx1 = end_node[0][0];	ny1 = end_node[0][1];
	nx2 = end_node[1][0];	ny2 = end_node[1][1];

	th = atan2((ny2-ny1),(nx2-nx1));
	L = pow((nx2-nx1),2) + pow((ny2-ny1),2);
	L = sqrt(L);
	for(i=0;i<n_segment-1;i++)
		t[i] = -1.0 + 2.0/n_segment*(i+1);

	/*
	nx1 = nx11*(1-t)/2 + nx22*(1+t)/2;
            nz1 = (1/8)*(4 - 6*t + 2*t^3)*0 + (L/8)*(t^2-1)*(t-1)*(alp1) + ...
                (1/8)*(4 + 6*t - 2*t^3)*0 + (L/8)*(t^2-1)*(t+1)*(alp2);
	
	  slope = (1/(4*L))*(6*(t^2 -1)*0 + L*(3*t^2 - 2*t -1)*alp1...
                + 6*(1-t^2)*0 + L*(3*t^2 + 2*t -1)*alp2);
	*/

	nx2 = nx2 - nx1;
	ny2 = ny2 - ny1;
	nx1 = nx2*cos(th) + ny2*sin(th);
	ny1 = ny2*cos(th) - nx2*sin(th);
	nx2 = nx1;	ny2 = ny1;
	nx1 = 0.0; ny1 = 0.0;
	for(i=0;i<n_segment-1;i++)
	{
		temp1 = nx1*(1-t[i])/2.0 + nx2*(1+t[i])/2.0;
		temp2 = (L/8.0)*((pow(t[i],2)-1)*(t[i]-1)*end_slope[0] + (pow(t[i],2)-1)*(t[i]+1)*end_slope[1]);
		mid_node[i][0] = temp1*cos(th) - temp2*sin(th) + end_node[0][0];
		mid_node[i][1] = temp1*sin(th) + temp2*cos(th) + end_node[0][1];
		temp1 = 1.0/4.0*((3.0*pow(t[i],2) - 2.0*t[i] -1)*end_slope[0]+
				(3.0*pow(t[i],2) + 2.0*t[i] -1)*end_slope[1]);
		mid_slope[i] = temp1;

	}
	free_dvector(t,0,n_segment-2);	

	
}



void new_freeblock(int **Telem, int TNelem,int *forcenode,int *fc_n1,int *extra_elem, int *extra_elem_n)
{
	int i,j,k,eye,jay,id=1,r_id=0,rt_id,dang_count,fc_n;
	int **group_id,*ID,**replace_id,*retain_id;

	fc_n = *fc_n1;
	group_id = imatrix(0,TNelem-1,0,1);
	replace_id = imatrix(0,TNelem-1,0,1);
	retain_id = ivector(0,TNelem-1);	
	ID = ivector(0,TNelem-1);

	for(i=0;i<TNelem;i++)
	{
		group_id[i][0] = 0;
		group_id[i][1] = 0;
		ID[i] = i;
		
	}
//printf("in new free block in remove duplicate TNelem = %d ,fc_n = %d \n",TNelem,fc_n);
	remove_duplicate(forcenode,&fc_n);
//printf("in new free block out remove duplicate\n");
	

//	sort_matrix_by_col(Telem_s,TNelem,3,ID,2);
	group_id[0][0] = Telem[0][0];		group_id[0][1] = 1;
	for(i=0;i<TNelem;i++)
	{
		eye = Telem[i][1];	jay = Telem[i][2];
		
		for(j=0;j<TNelem;j++)
		{
			if(i!=j)
			{
				
				if(eye == Telem[j][1] || eye == Telem[j][2] || jay == Telem[j][1] || jay == Telem[j][2])
				{
					if(group_id[i][1] == 0 && group_id[j][1]!=0)
					{
						group_id[i][1] = group_id[j][1];
						group_id[i][0] = Telem[i][0];
					}
					if(group_id[i][1] != 0 && group_id[j][1]==0)
					{
						group_id[j][1] = group_id[i][1];
						group_id[j][0] = Telem[j][0];
					}
					if(group_id[i][1] != 0 && group_id[j][1] != 0)
					{
						if(group_id[j][1] > group_id[i][1])
						{
							
							for(k=0;k<r_id;k++)
							{
								if(replace_id[k][0] == group_id[j][1])
								{
									if(replace_id[k][1] >group_id[i][1])
										replace_id[k][1] =group_id[i][1];

									break;
								}
							}
							if(k==r_id)
							{
								replace_id[r_id][0] = group_id[j][1];
								replace_id[r_id][1] = group_id[i][1];
								r_id++;
							}
							group_id[j][1] = group_id[i][1];
						}
						else if(group_id[j][1] < group_id[i][1])
						{
							
							for(k=0;k<r_id;k++)
							{
								if(replace_id[k][0] ==group_id[i][1])
								{
									if(replace_id[k][1] >group_id[j][1])
										replace_id[k][1] = group_id[j][1];

									break;
								}
							}
							if(k==r_id)
							{
								replace_id[r_id][0] = group_id[i][1];
								replace_id[r_id][1] = group_id[j][1];
								r_id++;
							}
							group_id[i][1] = group_id[j][1];
						}
						
					}
				}
			}
		}
		if(group_id[i][1] == 0)
		{
			id++;
			group_id[i][0] = Telem[i][0];
			group_id[i][1] = id;
		}
	}
/*

*/
//printf("in new free block in sort matrix\n");				
	sort_matrix_by_col(replace_id,r_id,2,ID,2);
	//printf("in new free block out sort matrix\n");				

	for(j=r_id-1;j>=0;j--)
	{
		for(i=0;i<TNelem;i++)
		{
			if(group_id[i][1] == replace_id[j][0])
				group_id[i][1] = replace_id[j][1];
		}
	}

	rt_id = 0;
	for(i=0;i<TNelem;i++)	
	{
		for(j=0;j<fc_n;j++)
		{
			if(forcenode[j] == Telem[i][1] || forcenode[j] == Telem[i][2])
			{
				retain_id[rt_id] = group_id[i][1];
				rt_id++;
				break;
				
			}
		}
	}
	k = 0;
	for(i=0;i<TNelem;i++)
	{
		for(j=0;j<rt_id;j++)
		{
			if(group_id[i][1] == retain_id[j])
				break;
		}
		if(j == rt_id)
		{
			extra_elem[k] = Telem[i][0];
			k++;
		}
	}
	*extra_elem_n = k;

	*fc_n1 = fc_n;
			

/*
	for(i=0;i<TNelem;i++)
		printf("%6d%6d%6d%6d\n",Telem[i][0],Telem[i][1],Telem[i][2],group_id[i][1]);

	for(i=0;i<r_id;i++)
		printf("%6d%6d\n",replace_id[i][0],replace_id[i][1]);
*/
	free_imatrix(group_id,0,TNelem-1,0,1);

	free_ivector(ID,0,TNelem-1);

//printf("in new free block in memory free %d %d\n",TNelem,rt_id);	
	free_imatrix(replace_id,0,TNelem-1,0,1);

	free_ivector(retain_id,0,TNelem-1);

	//printf("in new free block end\n");				
}


void dang_element(int **Telem, int TNelem,int *forcenode,int fc_n,int *dang_elem, int *dang_elem_n)
{
	int i,j,k;
	int eye,jay;

	k = 0;
	for(i=0;i<TNelem;i++)
	{
		eye = Telem[i][1];
		jay = Telem[i][2];
		for(j=0;j<TNelem;j++)
		{
			if(i!=j)
			{
				if(eye == Telem[j][1] || eye == Telem[j][2])
					eye = 0;
				if(jay == Telem[j][1] || jay == Telem[j][2])
					jay = 0;
				if(eye == 0 && jay == 0)
					break;
			}
		}
		if(eye != 0 || jay != 0)
		{
			for(j=0;j<fc_n;j++)
			{
				if(eye == forcenode[j] || jay == forcenode[j])
					break;
			}
			if(j == fc_n)
			{
				dang_elem[k] = Telem[i][0];
				k++;
			}
		}
	}
	*dang_elem_n = k;
}

int dof_check(int **elem,int Nelem,int Nnode,int *fix_node,int Nfix)
{
	int i,j,k,m;
	int nlink,njoint=0,dof,nl_ground,count_elem;
	int *ground_elem_id;

	ground_elem_id = new int [Nelem];
	
	nl_ground = 0;

	for(i=0;i<Nfix;i++)
	{
		for(j=i+1;j<Nfix;j++)
		{
			for(k=0;k<Nelem;k++)
			{
				if((elem[k][1] == fix_node[i] && elem[k][2] == fix_node[j]) ||
					(elem[k][1] == fix_node[i] && elem[k][2] == fix_node[j]) )
				{
					ground_elem_id[nl_ground] = k+1;
					nl_ground++;
				}
			}
		}
	}

	nlink = Nelem + 1 - nl_ground;

	m = 0;
	for(i=0;i<Nnode;i++)
	{
		count_elem = 0;
		for(j=0;j<Nelem;j++)
		{
			if(elem[j][1] == i+1 || elem[j][2] == i+1)
			{
				for(m=0;m<nl_ground;m++)
				{
					if(j+1 == ground_elem_id[m])
						break;
					
				}
				if(m == nl_ground)
					count_elem++;
			}
		}

		for(k=0;k<Nfix;k++)
		{
			if(i+1 == fix_node[k])
				break;
		}
		if(k<Nfix)
			njoint+=count_elem;
		else
			njoint+=(count_elem-1);

		

	}


	dof = 3*(nlink-1) - 2*njoint;

//	printf("%d  %d  %d \n",dof,nlink,njoint);

	

	return dof;
}