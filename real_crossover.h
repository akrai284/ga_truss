
//real crossover
void real_cross(int popsize, int nvar, int nchrom, double **select_pop,double **realpop,double pcross,double di1,double **lim_r)
{
	double parent1,parent2,child1,child2,r;
	double alpha,beta,betaq,y_lower,y_upper,y1,y2,expp;
	int i,j,y;
	if(nvar>0)
	{
		y = 0;
		for(i=0;i<popsize/2;i++)
		{
			r = rg.Random();
			if(r<pcross)
			{
				for(j=0;j<nvar;j++)
				{
					parent1 = select_pop[y][j];
					parent2 = select_pop[y+1][j];
					y_lower = lim_r[j][0];
					y_upper = lim_r[j][1];
					r = rg.Random();

					if(r<=0.5)
					{
						
						if(fabs(parent1 - parent2) > 0.000001)
						{
							if(parent2>parent1)
							{
								y1 = parent1;
								y2 = parent2;
							}
							else
							{
								y1 = parent2;
								y2 = parent1;
							}

							if((y1-y_lower)>(y_upper-y2))
							{
								beta = 1 + (2*(y_upper - y2)/(y2 - y1));
							}
							else
								beta = 1 + (2*(y1-y_lower)/(y2-y1));

							expp = di1 + 1.0;
							beta = 1.0/beta;
							alpha = 2.0 - pow(beta,expp);

							r = rg.Random();
							if(r<=1.0/alpha)
							{
								alpha = alpha*r;
								expp = 1.0/(di1+1.0);
								betaq = pow(alpha,expp);
							}
							else
							{
								alpha = alpha*r;
								alpha = 1.0/(2.0-alpha);
								expp = 1.0/(di1+1.0);
								if (alpha < 0.0) 
								{
								printf("ERRRORRR \n");
								exit(-1);
								}
								betaq = pow(alpha,expp);
							}
							/*Generating two children*/
							child1 = 0.5*((y1+y2) - betaq*(y2-y1));
							child2 = 0.5*((y1+y2) + betaq*(y2-y1));
						}
						else
						{
							betaq = 1.0;
							y1 = parent1;
							y2 = parent2;
							/*Generation two children*/
							child1 = 0.5*((y1+y2) - betaq*(y2-y1));
							child2 = 0.5*((y1+y2) + betaq*(y2-y1));
						}
						if (child1 < y_lower) child1 = y_lower;  //if childs are out of limits
						if (child1 > y_upper) child1 = y_upper;
						if (child2 < y_lower) child2 = y_lower;
						if (child2 > y_upper) child2 = y_upper;
					

					}
					else
					{
						child1 = parent1;
						child2 = parent2;
					
					}
					realpop[y][j]		= child1;
					realpop[y+1][j]		= child2;
					
				}
			}
			else
			{
				for(j=0;j<nvar;j++)
				{
					realpop[y][j]	 = select_pop[y][j];
					realpop[y+1][j]	 = select_pop[y+1][j];
				}		
			}
			y+=2;

		}
	}
}
/*
for(i=0;i<popsize;i++)
{
	for(j=0;j<nvar;j++)
		printf("\n %lf",realpop[i][j]);
getch();
}
*/
//binary crossover
void binary_cross(int popsize, int nvar, int nchrom,double **select_pop,double **binpop,double pcross)
{
	double r;
	int i,j,y,x;
	
		y =0;
		for(i=0;i<popsize/2;i++)
		{
		
			r = rg.Random();
			if(r<pcross)
			{
				r = rg.Random();
				x = floor(r*(nchrom+10));
				if(x >= nchrom)
					x = x/2;

				for(j = 0;j<nchrom;j++)
				{
					if(j>x-1)
					{
						binpop[y+1][j]	 = select_pop[y][j+nvar];
						binpop[y][j]	 = select_pop[y+1][j+nvar];
					}
					else
					{
						binpop[y][j]	 = select_pop[y][j+nvar];
						binpop[y+1][j]	 = select_pop[y+1][j+nvar];
					}
				}
			}
			else
			{
				for(j=0;j<nchrom;j++)
				{
					binpop[y][j]	= select_pop[y][j+nvar];
					binpop[y+1][j]	= select_pop[y+1][j+nvar];
				}
			}
			y+=2;
		}
	
}


void real_cross1(int popsize, int nvar,double **select_pop,double **realpop,double pcross, double **lim_r)
{
	double r,y_upper,y_lower,parent1,parent2,child1,child2,sigma1,sigma2;
	
	int i,j,y,partition;

	y=0;
	for(i=0;i<popsize/2;i++)
	{
		
		r = rg.Random();
		if(r<pcross)
		{
			r = rg.Random();
			partition = floor(r*popsize)+1;
			for(j=0;j<nvar;j++)
			{
				if(j<partition)
				{
					realpop[y][j] = select_pop[y+1][j];
					realpop[y+1][j] = select_pop[y][j];
				}
				else
				{
					realpop[y][j] = select_pop[y][j];
					realpop[y+1][j] = select_pop[y+1][j];
				}
				//UNDX
				parent1 = realpop[y][j];
				parent2 = realpop[y+1][j];
				y_lower = lim_r[j][0];
				y_upper = lim_r[j][1];
				if((parent1-y_lower)<(y_upper-parent1))
					sigma1 = parent1-y_lower;
				else
					sigma1 = y_upper-parent1;
				if((parent2-y_lower)<(y_upper-parent2))
					sigma2 = parent2-y_lower;
				else
					sigma2 = y_upper-parent2;



				r = rg.Random();
				child1 = -2.0*sigma1*log(r);
				r = rg.Random();
				if(r<0.5)
					child1 = parent1 + child1;
				else
					child1 = parent1 - child1;



				r = rg.Random();
				child2 = -2.0*sigma2*log(r);
				r = rg.Random();
				if(r<0.5)
					child2 = parent2 + child2;
				else
					child2 = parent2 - child2;

				
				if(child1<y_lower)
					child1 = y_lower;
				if(child1>y_upper)
					child1 = y_upper;
				if(child2<y_lower)
					child2 = y_lower;
				if(child2>y_upper)
					child2 = y_upper;
				realpop[y][j] = child1;
				realpop[y+1][j] = child2;
				

			}
		}
		else
		{
			for(j=0;j<nvar;j++)
			{
				realpop[y][j] = select_pop[y][j];
				realpop[y+1][j] = select_pop[y+1][j];
			}
		}
		y+=2;
	}


}


